
repo_dir:=$(dir $(abspath $(lastword $(MAKEFILE_LIST))))
bld_dir:=$(repo_dir)/build/rcg-build/
lib_bld_dir:=$(repo_dir)/build/cmake/
srcdir:=$(repo_dir)/advligorts
code_gen_dir:=$(srcdir)/src/epics/util
models_dir:= $(bld_dir)/models
bld_utils_dir:= $(bld_dir)/utils
NUM_BUILD_THREDS?=2

include $(repo_dir)/env/RCG.mk
#include $(repo_dir)/env/USERAPPS_ENV_VARS.mk

export RCG_SRC:=$(srcdir)/
export CDS_IFO_SRC=$(RCG_SRC)/src/include/
export RCG_LIB_PATH:=:$(srcdir)/src/epics/simLink/:$(srcdir)/src/epics/simLink/lib/:$(srcdir)/test/:$(repo_dir)/models/:$(file < $(repo_dir)/env/RCG_LIB_PATH.env)
export RCG_TARGET:=$(bld_dir)/ipc/

define DIE
(tail $(bld_log); cat $(err_log) && /bin/false)
endef

%:

	@# Set path variables so we don't need the whole path everywhere
	$(eval model_bld_dir := $(bld_dir)/models/$@/)
	$(eval target_dir := $(model_bld_dir)/target/)
	$(eval model_log_dir := $(bld_dir)/models/$@/logs)
	$(eval err_log := $(model_log_dir)/$@_error.log)
	$(eval bld_log := $(model_log_dir)/$@.log)
	$(eval kern_bld_dir := $(model_bld_dir)/kernel_mod/)
	$(eval us_bld_dir := $(model_bld_dir)/userspace/)
	$(eval epics_dir := $(model_bld_dir)/epics/)
	$(eval epics_src_dir := $(epics_dir)/src/)
	$(eval epics_build_dir := $(target_dir)/$@epics/)
	$(eval bld_info_dir := $(target_dir)/build_info/)
	$(eval target_bin_dir := $(target_dir)/bin/)


	@rm -rf $(model_log_dir) #Clear out logs
	@mkdir -p $(lib_bld_dir) $(bld_dir) $(bld_utils_dir)/epics/util $(models_dir) $(epics_dir)/fmseq $(models_dir)/$@/include $(model_log_dir)
	@mkdir -p $(target_dir)/ $(target_bin_dir)/
	@ln -fs $(srcdir)/src/epics/util/lib $(bld_utils_dir)/epics/util > /dev/null 2>&1 || /bin/true
	@ln -fs $(srcdir)/src/epics/simLink $(bld_utils_dir)/epics > /dev/null 2>&1 || /bin/true
	@ln -fs $(srcdir)/src/epics/util/Makefile.kernel $(bld_utils_dir)/epics/util > /dev/null 2>&1 || /bin/true
	@ln -fs $(srcdir)/src/epics/util/Userspace_CMakeLists.cmake $(bld_utils_dir)/epics/util > /dev/null 2>&1

	@echo Parsing the model $@...
	@(cd $(bld_utils_dir)/epics/util && env SIMULATION_BUILD=1 env RCG_SRC_DIR=$(srcdir) PERL5LIB=$(code_gen_dir) $(code_gen_dir)/feCodeGen.pl $@.mdl $@ >> $(bld_log) 2>>$(err_log)) || $(DIE)
	@echo Done
	@/bin/rm -rf $(epics_dir)/$@epics-medm
	@/bin/rm -rf $(epics_dir)/$@epics-config
	@/bin/mv -f  $(epics_src_dir)/medm $(epics_dir)/$@epics-medm
	@/bin/mv -f  $(epics_src_dir)/config $(epics_dir)/$@epics-config
	@/bin/rm -rf $(epics_build_dir)/ $(epics_src_dir)/;

	@echo Building front-end user space object $@...
	@if [ ! -L $(us_bld_dir)/src ]; then ln -s $(srcdir)/src/ $(us_bld_dir)/src; fi
	@mkdir -p $(us_bld_dir)/build
	@cmake -B$(us_bld_dir)/build/ -S$(us_bld_dir) -DDEFAULT_TO_SHMEM_BUFFERS=1 -DMODEL_NAME=$@ >> $(bld_log) 2>>$(err_log) || $(DIE)
	@make -j $(NUM_BUILD_THREDS) -C $(us_bld_dir)/build >> $(bld_log) 2>>$(err_log) || $(DIE)
	@./scripts/buildMapFromHeader.py $(model_bld_dir)/include/$@.h $(model_bld_dir)/epics/fmseq/$@ $(model_bld_dir)/include/var_lookup_table.h  >> $(bld_log) 2>>$(err_log) || $(DIE)
	@echo Done

	@echo Building rtsLib for the model $@...
	@cmake -B $(lib_bld_dir) -S$(repo_dir) -DMODEL_NAME=$@ >> $(bld_log) 2>>$(err_log) || $(DIE)
	@make -j $(NUM_BUILD_THREDS) -C $(lib_bld_dir) >> $(bld_log) 2>>$(err_log) || $(DIE)
	@echo Done

	@echo RCG source code directory:
	@echo $(srcdir)
	@echo The following files were used for this build:
	@sort $(epics_dir)/sources.$@ | uniq
	@echo
	@echo  Successfully compiled $@
	@echo '***********************************************'
	@echo  Code Generation Warnings, found in $(model_log_dir)/$@_warnings.log:
	@echo '***********************************************'
	@cat   $(model_log_dir)/$@_warnings.log 
	@echo '***********************************************'
	@echo  Code Compilation Warnings/Errors, found in $(model_log_dir)/$@_error.log:
	@echo '***********************************************'
	@cat   $(model_log_dir)/$@_error.log
	@echo '***********************************************'


.DEFAULT: all
.PHONY: clean all test install-modules-from-source rmmod insmod check

all:
	@echo "Please enter the name of the model you would like to build"
	@false

clean:
	rm -r $(repo_dir)/build

check:
	@if ( git submodule status |  wc -w | grep "3" > /dev/null ) ; then echo "Check: advligorts submodule initialized!"; else echo "Error: advligorts submodule NOT initialized"; fi;

test:
	cd $(lib_bld_dir) && ctest


