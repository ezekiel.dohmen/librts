# Lib RTS
The RTS library allows users to build advLIGO *models*, ie. `.mdl` files created with Simulink/cdsParts library, simulate inputs and capture outputs. It provides a c++ interface, as well as python bindings for use in either language. 

## Cloning this Repository
Because this repository uses [advligorts](https://git.ligo.org/cds/software/advligorts), you need to clone submodules too.


#### SSH
```
    git clone --recurse-submodules git@git.ligo.org:ezekiel.dohmen/rtslib.git
```

#### HTTPS
```
    git clone --recurse-submodules https://git.ligo.org/ezekiel.dohmen/rtslib.git
```

## Dependencies 

### Required
The required packages that should be installed from your distribution's repositories are:
```
build-essential cmake perl libswitch-perl libspdlog-dev catch python3-dev git python3-pybind11
```

### Others
Some python scrips have ***python dependencies*** that you will need to run them
```
numpy, matplotlib 
```

## Directory Structure

| Directory | Uses |
| ---------------------- | ----------------------------------------- |
| `advligorts/` | This is the [advligorts](https://git.ligo.org/cds/software/advligorts) submodule that has the RCG and model library build scripts |
| `docker/`     | Holds docker scripts showing how this repository might be cloned, built and run on a system |
| `env/`        | Contains config scripts holding environmental variables sourced bu the build scripts |
| `examples`    | C++ examples on how to use the `librts` interface |
| `include/`    | `librts` header files |
| `models/`     | Directory that is included in the search path for models (`.mdl` files) |
| `python/`     | python pybind11 binding files and python example scripts on using the python interface |
| `scripts/`    | Utility scripts for plotting c++ output and finding model search paths |
| `src/`        | `librts` source files |
| `tests/`      | C++ tests ran against `librts` |

## Building Custom Models
Simple custom models are very simple to build. The general steps are to make sure this repository's `Makefile` will be able to find your `.mdl` file. The simplest way is to just copy it into the `models/` directory, ex: `cp x1testmodel.mdl /home/$USER/git/rtsLib/models/` and then run make <model_name> from the repository directoy. ex: `cd  /home/$USER/git/rtsLib/; make x1testmodel`.

## Building Production Models

    (In progress)

Some production models have hard coded paths to `/opt/rtcds/userapps/release/` so you are going to need to setup a symlink from that path to your userapps or install userapps to that standard location.

## Running Python Examples
All you should need to do is build the model, set your `PYTHONPATH`, and run the script. Ex (from the repo root dir):
```
~/git/rtslib$ make x1unittest
~/git/rtslib$ source ./scripts/setDefaultPythonPath.sh
~/git/rtslib$ ./python/examples/load-module-example.py
```

## Parts Notes
### IPCs
IPCs dont work the same way with `librts`, because external sender/receiver models are not running. Some special considerations should be made. 
| IPC Setup     | Status | Notes |
| ----------- | ----------- | ------------ |
| Sender and receiver in same model      | SUPPORTED       | Works as it would with a real time model deployment |
| Receiver only   | SUPPORTED      | Because there is no "sender" model running, the output of these can be configured with a Generator |
| Sender only   | SUPPORTED       | Sender IPCs that would normally be sent to another model can be collected by recording and trending the IPC like any other recordable variable |

## Generator Objects
Many parts (excitations, ADCs, IPC receivers, etc.) can have their outputs simulated by a `Generator` object. The C++ library supports a number of generators, and more can be easily added. Checkout `include/RampGenerator.hh` for an example of a C++ Generator, and `python/examples/generators-example.py` for usage in python, as well as how you would define a custom Generator in python.  

### Currently Implemented Generators
| Name     |            Function |   Prototype |
| -------  | ------------------- | ----------- |
| `CannedSignalGenerator` | The output is simulated by reading samples from the provided list. If repeat is set to false, when you run out of canned samples the generator will return 0 on successive calls. If repeat is true, the buffer is replayed from the first sample each time the end is reached. | `CannedSignalGenerator(const std::vector<double>& canned_data, bool repeat = true)` |
| `ConstantGenerator` | Feeds the constant value configured as output | `ConstantGenerator(double value)` |
| `GaussianNoiseGenerator` | Simulates Gassian noise with the configured parameters | `GaussianNoiseGenerator(double mean, double stddev) ` |mak
| `RampGenerator` | Simulates a ramp output with the configured parameters | `RampGenerator(double start_val, double step)` |
| `UniformRealGenerator` | Generates real numbers in the range [a, b) | `UniformRealGenerator(double min, double max)` |


## TODO
- We might be able to expose more model variables, maybe even all edges, but right now some of those are static and others don't appear in the <model_name> header.

## Debugging 
You can turn on more verbose logging by changing the spdlog level:
```
export SPDLOG_LEVEL=info
(run your app)
``` 
