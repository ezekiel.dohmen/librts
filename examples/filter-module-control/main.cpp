#include "Model.hh"

#include <iostream>
#include <fstream>

int main(int argc, char** argv)
{

    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    if(model_ptr == nullptr)
    {
        std::cout << "Error coult not create Model. Exiting..."  << std::endl;
        return 2;
    }

    //Set inputs 
    //


    //Configure modules
    //
    std::optional<rts::LIGO_FilterModule> filter1 = model_ptr->get_filter_module_by_name("FM0");
    filter1.value().enable_input().enable_output().set_gain(1).set_ramp_time_s(0.0).enable_stages( {0} );
    
    model_ptr->load_biquad_coefficients("FM0", {0}, { 0.56593403, -0.86813193, -0.86813193, -0.86813193, -0.86813193});


    //Configure record points
    //
    model_ptr->record_dac_output(true);

    //Run Model
    //
    model_ptr->run_model(model_ptr->get_model_rate_Hz()*1);

    //Examine Outputs
    const std::vector<double> & dac_chan = model_ptr->get_dac_output_by_id(0,0);
    auto myfile = std::fstream("dac_0_0.doubles", std::ios::out | std::ios::binary);
    myfile.write((char*)&dac_chan[0], dac_chan.size() * sizeof(double));
    myfile.close();


    return 0;
}
