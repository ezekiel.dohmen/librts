#include "Model.hh"

#include "RampGenerator.hh"

#include <iostream>
#include <fstream>

int main(int argc, char** argv)
{

    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    if(model_ptr == nullptr)
    {
        std::cout << "Error coult not create Model. Exiting..."  << std::endl;
        return 2;
    }

    /*
    std::vector<std::string> names = model_ptr->getAllInputNames();
    for (auto& adc : names)
    {
        std::cout << adc  << std::endl;
    }*/


    std::vector<std::string> filter_names = model_ptr->get_all_filter_names();
    for (auto& filter_name : filter_names)
    {
        std::cout << filter_name  << std::endl;
    }


    //Set inputs 
    //
    model_ptr->set_adc_channel_generator( 0, 31, std::make_unique<rts::RampGenerator>(0, 1) ); 

    //Configure modules
    //
    model_ptr->set_var("EX_EPICS_BIN_IN", 5);
    std::cout << model_ptr->get_var<int>("EX_EPICS_BIN_IN").value() << std::endl;

    std::optional<rts::LIGO_FilterModule> filter1 = model_ptr->get_filter_module_by_name("FM0");
    filter1.value().enable_input().enable_output().enable_offset(5);

    //Configure record points
    //
    model_ptr->record_dac_output(true);

    //Run Model
    //
    model_ptr->run_model(model_ptr->get_model_rate_Hz()*10);
    //Maybe change config
    //model_ptr->...()
    //Run some more
    //model_ptr->runModel(1024);

    //Examine Outputs
    const std::vector<double> & dac_chan = model_ptr->get_dac_output_by_id(0,0);
    auto myfile = std::fstream("dac_0_0.doubles", std::ios::out | std::ios::binary);
    myfile.write((char*)&dac_chan[0], dac_chan.size() * sizeof(double));
    myfile.close();

    std::cout << model_ptr->get_var<int>("EX_EPICS_BIN_IN").value() << std::endl;


    return 0;
}
