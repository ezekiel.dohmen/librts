#include "Model.hh"

#include "RampGenerator.hh"

#include <iostream>
#include <fstream>

#include "spdlog/cfg/env.h"

int main(int argc, char** argv)
{

    spdlog::cfg::load_env_levels();


    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    if(model_ptr == nullptr)
    {
        std::cout << "Error coult not create Model. Exiting..."  << std::endl;
        return 2;
    }


    //Set inputs 
    //
    model_ptr->set_adc_channel_generator( 0, 30, std::make_unique<rts::RampGenerator>(0, 1) ); 

    //Configure modules
    //
    //model_ptr->recordModelVar("RAMP_EPICS_OUT", model_ptr->getModelRate_Hz());
    model_ptr->record_model_var("RAMP_EPICS_OUT", 10);


    //Configure record points
    //
    model_ptr->record_dac_output(false);

    //Run Model
    //
    model_ptr->run_model(model_ptr->get_model_rate_Hz()*1);//Run for 1 sec

    //Examine Recorded Outputs
    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("RAMP_EPICS_OUT");
    if ( ! epics_out)
    {
        std::cout << "Error could not get recorded data." << std::endl;
        return 0;
    }
    auto myfile = std::fstream("epics_out.doubles", std::ios::out | std::ios::binary);
    myfile.write((char*)&epics_out.value()[0], epics_out.value().size() * sizeof(double));
    myfile.close();



    return 0;
}
