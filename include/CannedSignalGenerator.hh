#ifndef LIGO_CANNED_SIGNAL_GENERATOR_HH
#define LIGO_CANNED_SIGNAL_GENERATOR_HH

#include "Generator.hh"

#include <vector>
#include <stdexcept>

namespace rts
{
    class CannedSignalGenerator : public Generator
    {
        public:
            CannedSignalGenerator(const std::vector<double>& canned_data, bool repeat = true)
                : _cur_inx(0), _repeat(repeat), _canned_data(canned_data)
            {
            }
            virtual double get_next_sample()
            {
                if(_repeat == false &&_cur_inx == _canned_data.size() - 1)
                {
                    return 0.0;
                }
                double hold = _canned_data[_cur_inx];
                _cur_inx = (_cur_inx + 1) %  _canned_data.size();
                return hold;
            }
            virtual void reset()
            {
                _cur_inx = 0;
            }
            virtual ~CannedSignalGenerator() {};
        private:

            uint64_t _cur_inx;
            bool _repeat;
            std::vector<double> _canned_data;

    };
}




#endif //LIGO_CANNED_SIGNAL_GENERATOR_HH
