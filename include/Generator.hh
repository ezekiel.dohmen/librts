#ifndef LIGO_GENERATOR_HH
#define LIGO_GENERATOR_HH

namespace rts
{

    class Generator
    {
        public:
            Generator() {};

            virtual double get_next_sample() = 0;
            virtual void reset() = 0;
            virtual ~Generator() = default;
        private:
    };

}


#endif //LIGO_GENERATOR_HH
