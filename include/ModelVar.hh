#ifndef LIGO_MODEL_VAR_HPP
#define LIGO_MODEL_VAR_HPP

#define MAX_VAR_LEN 128

#include <string>
#include <cmath>

namespace rts
{
    enum SUPPORTED_TYPES
    {
        INT = 0,
        DOUBLE = 1,
        UNUSED1 = 2,
        UNUSED2 = 3,
        UNUSED3 = 4,
        CHAR = 5
    }; //Enums are in namespace so common names should be fine

    std::string SUPPORTED_TYPES_NAMES [] =
    {
        "int",
        "double",
        "unused1",
        "unused2",
        "unused3",
        "char"
    };

    inline std::string get_type_string( SUPPORTED_TYPES val)
    {
        switch ( val )
        {   
            case INT:
                return "int";
                break;
            case DOUBLE:
                return "double";
                break;
            case CHAR:
                    return "char";
                break;
            default:
                return "UNKNOWN TYPE";
        }
    }

    struct model_var_lookup_t
    {
        char var_name[MAX_VAR_LEN];
        void * val_ptr;
        SUPPORTED_TYPES type;
    };

    struct filter_module_lookup_t
    {
        char filter_name[MAX_VAR_LEN];
        int module_index;
    };

    struct test_point_lookup_t
    {
        char name[MAX_VAR_LEN];
        int index;
    };

    struct excitation_point_lookup_t
    {
        char name[MAX_VAR_LEN];
        int index;
    };

    
    class ModelVarLocation
    {
        public:
            explicit ModelVarLocation(void* var_ptr, SUPPORTED_TYPES type)
                                     : _var_ptr(var_ptr), _type(type) {};

            void * get_ptr() { return _var_ptr; };
            SUPPORTED_TYPES get_type() const { return _type; };

            template <class T>
            T get_val() const
            {
                switch ( _type )
                {
                    case INT:
                        return *( static_cast<int*>( _var_ptr ) );
                        break;
                    case DOUBLE:
                        return *( static_cast<double*>( _var_ptr ) );
                        break;
                    case CHAR:
                        return *( static_cast<char*>( _var_ptr ) );
                        break;
                    default:
                        return 0; //TODO, should we signal error
                }
            }

            void set_val(double val) 
            {
                switch ( _type )
                {
                    case INT:
                        *static_cast<int*>( _var_ptr ) = lround( val );
                        break;
                    case DOUBLE:
                        *static_cast<double*>( _var_ptr ) = val;
                        break;
                    case CHAR:
                        *static_cast<char*>( _var_ptr ) = lround( val );;
                        break;
                    default:
                        return; 
                }
            }

        private:
            void * _var_ptr;
            SUPPORTED_TYPES _type;
    };

    class ModelVarValue
    {

        public:

        union ModelVarValue_t
        {
            int asInt;
            double asDouble;
            char asChar;
        };

        ModelVarValue_t value;

        template <class reqType>
        reqType get_val(SUPPORTED_TYPES type) const
        {
              switch (type)
                {
                    case INT:
                        return static_cast< reqType >( *( reinterpret_cast< const int *>( &value  ) ) );
                        break;
                    case DOUBLE:
                        if( std::is_integral<reqType>::value) //If underlying type is double and we are returning integral type, then round
                            return std::lrint( *( reinterpret_cast< const double *>( &value  ) ) ) ;
                        else
                            return static_cast< reqType >( *( reinterpret_cast< const double *>( &value  ) ) );
                        break;
                    case CHAR:
                        return static_cast< reqType >( *( reinterpret_cast< const char *>( &value  ) ) );
                        break;
                    default:
                        return 0;
                }
        }

    };



}


#endif
