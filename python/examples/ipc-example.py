#!/usr/bin/env python3
import x1unittest_pybind as model #Built by rtslib
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sig


mod = model.create_instance()

mod.set_ipc_receiver_generator( "X1:RECV_ONLY", model.RampGenerator(startVal=0, step=1.0))


# Set up variable recording
mod.record_model_var("IPC_RECV_ONLY_TP", mod.get_model_rate_Hz()) #record Gaussian noise at model rate


mod.run_model( int(mod.get_model_rate_Hz() * 1) ) #Run for 1 second

# Trend DAC card 0 chan 0 over the simulation
var_output = mod.get_recorded_var("IPC_RECV_ONLY_TP"); 


p1 = plt.figure(1)
plt.plot(var_output)
plt.title("IPC Receiver from external model sender")


plt.show()

