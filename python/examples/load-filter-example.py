#!/usr/bin/env python3
import x1unittest_pybind as model #Built by rtslib
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sig


mod = model.create_instance()

mod.set_adc_channel_generator(0, 0, model.GaussianNoiseGenerator(mean=0, stdDev=1))

filter1 = mod.get_filter_module_by_name("FM0");
filter1.enable_input().enable_output().set_gain(1).set_ramp_time_s(0.0).enable_stages( [0] );

zpk = sig.bilinear_zpk(-2*np.pi*np.array([0]), -2*np.pi*np.array([4000]), 1, mod.get_model_rate_Hz())
sos_coef = sig.zpk2sos(*zpk)
mod.load_scipy_sos_coef("FM0", [0], sos_coef)

mod.record_dac_output(True) #This allows up to lookup DAC output after a call to run Model

mod.run_model( mod.get_model_rate_Hz()*1 ) #Run for 1 second

dac_output = mod.get_dac_output_by_id(0, 0); #Get card 0, chan 0 output

# Trend DAC card 1 chan 1 over the second simulation
p1 = plt.figure(1)
plt.plot(dac_output)
plt.title("Filtered TD Samples")

p2 =  plt.figure(2)
plt.plot(10*np.log(np.abs(np.fft.rfft(dac_output))))
plt.title("Mag of Real Side FFT")
plt.show()


