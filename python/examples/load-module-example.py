#!/usr/bin/env python3
import x1unittest_pybind as model #Built by rtslib
import matplotlib.pyplot as plt


mod = model.create_instance()

#
# Print out general information about the model
#
print(f"Model rate (Hz) : { mod.get_model_rate_Hz() }\n" )

print("All filter names :")
print(mod.get_all_filter_names(), end="\n\n")

print("All ADC/DACs: ")
print(mod.get_all_input_names(), end="\n\n")

print("All controllable model variable names:")
print(mod.get_all_model_var_names(), end="\n\n")
#
# Done printing info about the model

# Configure the filter module
filter1 = mod.get_filter_module_by_name("FM0");
filter1.enable_input().enable_output().set_gain(1).set_ramp_time_s(0.5).enable_stages( [0] );

# Configure ADC input
mod.set_adc_channel_generator(0, 0, model.GaussianNoiseGenerator(mean=0, stdDev=1))

# Setting this option allows the user to lookup DAC output after a call to run Model
mod.record_dac_output(True)

#Can set a model var a few ways
mod.set_var("RAMP_EPICS_OUT", 5.0)
mod["RAMP_EPICS_OUT"] = 5.0;

print(mod.get_var("RAMP_EPICS_OUT"))

mod.run_model( mod.get_model_rate_Hz()*1 ) #Run for 1 second

dac_output = mod.get_dac_output_by_id(0, 0);

# Trend DAC card 1 chan 1 over the second simulation
p1 = plt.figure(1)
plt.plot(dac_output)
plt.title("DAC 0, chan 0, Time Domain Samples")
plt.show()
