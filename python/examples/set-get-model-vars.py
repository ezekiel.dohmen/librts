#!/usr/bin/env python3
import x1unittest_pybind as model #Built by rtslib
import matplotlib.pyplot as plt


mod = model.create_instance()

# Example setting/getting double values
mod.set_var("RAMP_EPICS_OUT", 5.12)
mod["DAC_15_EPICS_INPUT"] = 22.222

print( mod["RAMP_EPICS_OUT"] )
print( mod.get_var("DAC_15_EPICS_INPUT") )
print( mod["DAC_15_EPICS_INPUT"] )


# Example setting/getting int values
mod.set_var("EX_EPICS_BIN_IN", 48374635)
print(mod.get_var_int("EX_EPICS_BIN_IN"))



# Example setting/getting char value (advligo mask types)
mod["DAC_15_EPICS_INPUT_MASK"] = 0x71 #ASCII 'q'
print(f'0x{mod.get_var_byte("DAC_15_EPICS_INPUT_MASK").hex()}')

