#!/usr/bin/env python3
import x1unittest_pybind as pyrts #Built by rtslib
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sig


mod = pyrts.Model.create_instance()

testpoint_name = "TP_TEST"

mod.set_adc_channel_generator(0, 31, pyrts.RampGenerator(startVal=0, step=1.0))

mod.get_filter_module_by_name("FM31").enable_input().enable_output().set_gain(1).set_ramp_time_s(0.0);

# Set up variable recording
mod.record_model_var(testpoint_name, mod.get_model_rate_Hz()) 


mod.run_model( mod.get_model_rate_Hz() * 1 ) #Run for 1 second

# Trend DAC card 0 chan 0 over the simulation
var_output = mod.get_recorded_var(testpoint_name); 

p1 = plt.figure(0)
plt.plot(var_output, '#008000')
plt.title("Testpoint: " + testpoint_name)
plt.show()

