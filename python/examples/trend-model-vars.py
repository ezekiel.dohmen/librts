#!/usr/bin/env python3
import x1unittest_pybind as model #Built by rtslib
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sig


mod = model.create_instance()

mod.set_adc_channel_generator(0, 26, model.RampGenerator(startVal=0, step=1.0))
mod.set_adc_channel_generator(0, 28, model.GaussianNoiseGenerator(mean=0, stdDev=1))

mod.get_filter_module_by_name("FM26").enable_input().enable_output().set_gain(1).set_ramp_time_s(0.0);
mod.get_filter_module_by_name("FM28").enable_input().enable_output().set_gain(1).set_ramp_time_s(0.0);

# Set up variable recording
mod.record_dac_output(True) #This allows up to lookup DAC output after a call to run Model
mod.record_model_var("DAC_13_EPICS_OUT", int(mod.get_model_rate_Hz()/64)) #record the ramp signal at 1/64th the model rate
mod.record_model_var("DAC_14_EPICS_OUT", mod.get_model_rate_Hz()) #record Gaussian noise at model rate


mod.run_model( int(mod.get_model_rate_Hz() * 1) ) #Run for 1 second

# Trend DAC card 0 chan 0 over the simulation
var_output = mod.get_recorded_var("DAC_13_EPICS_OUT"); 
dac_output = mod.get_dac_output_by_id(0, 13)

# Get the samples var value and plot it with the DAC getting the data at the full rate
p1 = plt.figure(0)
plt.scatter( list(range(0, len(dac_output), 64)), var_output) #Here we adjust the x scale so it aligns with the full rate dac output
plt.plot(dac_output, '#008000')
plt.title("Card 0, Chan 13")

var_output = mod.get_recorded_var_int("DAC_14_EPICS_OUT"); #we get the Gaussian noise as int and plot against DAC output
dac_output = mod.get_dac_output_by_id(0, 14)
p1 = plt.figure(1)
plt.plot(var_output)
plt.plot(dac_output, '#008000')
plt.title("Card 0, Chan 14")




plt.show()

