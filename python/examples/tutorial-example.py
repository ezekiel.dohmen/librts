#!/usr/bin/env python3
import x1tutorial_pybind as model #Built by rtslib
import matplotlib.pyplot as plt


mod = model.create_instance()

mod["NUM_SAMPLES_WHEN_TRIG"] = 100
#mod.set_excitation_point_generator("NUM_SAMPLES_WHEN_TRIG", model.ConstantGenerator( value=100.0 ))
mod["PROB_OF_SIGNAL"] = 0.002
#mod.set_excitation_point_generator("PROB_OF_SIGNAL", model.ConstantGenerator( value=0.002 ))

mod["PULSE_OSC_FREQ"] = 500
mod["PULSE_OSC_SINGAIN"] = 100

mod.record_model_var("CUR_COUNT_TP", mod.get_model_rate_Hz()) 
mod.record_model_var("IS_COUNTING_TP", mod.get_model_rate_Hz())
mod.record_model_var("COUNT_TO_TP", mod.get_model_rate_Hz()) 
mod.record_model_var("RESET_TP", mod.get_model_rate_Hz()) 
mod.record_model_var("SIGNAL_TP", mod.get_model_rate_Hz()) 


mod.run_model(mod.get_model_rate_Hz() * 1)


# 
counter_output = mod.get_recorded_var("CUR_COUNT_TP")
is_counting_tp = mod.get_recorded_var("IS_COUNTING_TP")
count_to_tp = mod.get_recorded_var("COUNT_TO_TP")
reset_tp = mod.get_recorded_var("RESET_TP")
signal_tp = mod.get_recorded_var("SIGNAL_TP")




# Get the samples var value and plot it with the DAC getting the data at the full rate
p0 = plt.figure(0)
plt.plot(count_to_tp, '#008000')
plt.title("countTo")

p1 = plt.figure(1)
plt.plot(reset_tp, '#008000')
plt.title("reset")

p2 = plt.figure(2)
plt.plot(is_counting_tp)
plt.title("isCounting")

p3 = plt.figure(3)
plt.plot(counter_output)
plt.title("curCount")

p4 = plt.figure(4)
plt.plot(signal_tp)
plt.title("SIGNAL_TP")

plt.show()
