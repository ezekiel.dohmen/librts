#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "Model.hh"
#include "LIGO_FilterModule.hh"

//Generators
#include "CannedSignalGenerator.hh"
#include "ConstantGenerator.hh"
#include "GaussianNoiseGenerator.hh"
#include "RampGenerator.hh"
#include "UniformRealGenerator.hh"

namespace py = pybind11;

using namespace rts;

// 
// This is a required trampoline class that allows us to define
// Generator classes in python and then have the underlying c++
// runtime use them 
// 
class PyGenerator : public rts::Generator {
    public:
        /* Inherit the constructors */
        using Generator::Generator;

        /* Trampoline (need one for each virtual function) */
        double get_next_sample() override {
            PYBIND11_OVERRIDE_PURE(
                double, /* Return type */
                rts::Generator,      /* Parent class */
                get_next_sample,          /* Name of function in C++ (must match Python name) */
            );
        }

        void reset() override {
            PYBIND11_OVERRIDE_PURE(
                void, /* Return type */
                rts::Generator,      /* Parent class */
                reset,          /* Name of function in C++ (must match Python name) */
            );
        }
};

PYBIND11_MODULE(PYBIND_MODULE_NAME, m) { //PYBIND_MODULE_NAME is defined by the build scripts

    m.def("create_instance", &rts::Model::create_instance);

    py::class_<rts::Model>(m, "Model")
        .def("create_instance", &rts::Model::create_instance)
        .def("get_model_rate_Hz", &rts::Model::get_model_rate_Hz)
        .def("get_num_filter_modules", &rts::Model::get_num_filter_modules)
        .def("get_all_input_names", &rts::Model::get_all_input_names)
        .def("get_all_model_var_names", &rts::Model::get_all_model_var_names)
        .def("set_var", static_cast<bool (rts::Model::*)(const std::string &, double)>(&rts::Model::set_var) )
        .def("set_var", static_cast<bool (rts::Model::*)(const std::string &, int)>(&rts::Model::set_var) )
        .def("set_var_momentary", &rts::Model::set_var_momentary)
        .def("get_var", &rts::Model::get_var<double>) //Default get_var() is double
        .def("get_var_int", &rts::Model::get_var<int>)
        .def("get_var_double", &rts::Model::get_var<double>)
        .def("get_var_byte", 
            [](const rts::Model &s, const std::string key) {
            std::optional<char> hold = s.get_var<char>(key);
            if ( !hold ) throw py::key_error();
            return py::bytes(&hold.value(), 1); 
        })
        .def("record_model_var", &rts::Model::record_model_var)
        .def("get_recorded_var", &rts::Model::get_recorded_var<double>)
        .def("get_recorded_var_double", &rts::Model::get_recorded_var<double>)
        .def("get_recorded_var_int", &rts::Model::get_recorded_var<int>)
        .def("get_recorded_var_bytes", 
            [](const rts::Model &s, const std::string key) {
            std::optional<char> hold = s.get_var<char>(key);
            if ( !hold ) throw py::key_error();
            return py::bytes(&hold.value(), 1); 
        })

        .def("get_all_filter_names", &rts::Model::get_all_filter_names)
        .def("get_filter_module_by_name", &rts::Model::get_filter_module_by_name)
        .def("get_filter_module_by_id", &rts::Model::get_filter_module_by_id)
        .def("reset_filter_module", &rts::Model::reset_filter_module)
        .def("load_biquad_coefficients", &rts::Model::load_biquad_coefficients)
        .def("load_scipy_sos_coef", &rts::Model::load_scipy_sos_coef)

        .def("set_adc_channel_generator", &rts::Model::set_adc_channel_generator)
        .def("set_excitation_point_generator", &rts::Model::set_excitation_point_generator)
        .def("set_ipc_receiver_generator", &rts::Model::set_ipc_receiver_generator)
        .def("record_dac_output", &rts::Model::record_dac_output)
        .def("get_dac_output_by_id", &rts::Model::get_dac_output_by_id, py::arg("card"), py::arg("chan"))
        .def("run_model", &rts::Model::run_model)
        .def("__getitem__",
             [](const rts::Model &s, const std::string name) {
                 std::optional<double> res = s.get_var<double>(name);
                 if( ! res ) throw py::key_error();
                 return res.value();
             })
        .def("__setitem__",
             [](rts::Model &s, const std::string name, double value) {
                 if ( !s.set_var(name, value) ) throw py::key_error();
             })
        ;



    py::class_<rts::LIGO_FilterModule>(m, "LIGO_FilterModule")
        .def("enable_input", &rts::LIGO_FilterModule::enable_input)
        .def("disable_input", &rts::LIGO_FilterModule::disable_input)
        .def("enable_output", &rts::LIGO_FilterModule::enable_output)
        .def("disable_output", &rts::LIGO_FilterModule::disable_output)
        .def("enable_offset", &rts::LIGO_FilterModule::enable_offset)
        .def("disable_offset", &rts::LIGO_FilterModule::disable_offset)
        .def("enable_limiter", &rts::LIGO_FilterModule::enable_limiter)
        .def("disable_limiter", &rts::LIGO_FilterModule::disable_limiter)
        .def("enable_hold", &rts::LIGO_FilterModule::enable_hold)
        .def("disable_hold", &rts::LIGO_FilterModule::disable_hold)
        .def("set_gain", &rts::LIGO_FilterModule::set_gain)
        .def("set_ramp_time_s", &rts::LIGO_FilterModule::set_ramp_time_s)
        .def("enable_stages", &rts::LIGO_FilterModule::enable_stages)
        .def("disable_stages", &rts::LIGO_FilterModule::disable_stages)
        ;


    py::class_<rts::Generator, PyGenerator, std::shared_ptr<Generator> >(m, "Generator")
    .def(py::init<>())
    .def("get_next_sample", &rts::Generator::get_next_sample)
    .def("reset", &rts::Generator::reset);


    py::class_<rts::CannedSignalGenerator, rts::Generator, std::shared_ptr<rts::CannedSignalGenerator> >(m, "CannedSignalGenerator")
        .def(py::init<const std::vector<double>&, bool>(), "constructor", py::arg("canned_data"), py::arg("repeat") = true)
        .def("get_next_sample", &rts::CannedSignalGenerator::get_next_sample)
        .def("reset", &rts::CannedSignalGenerator::reset);

    py::class_<rts::ConstantGenerator, rts::Generator, std::shared_ptr<rts::ConstantGenerator> >(m, "ConstantGenerator")
        .def(py::init<double>(), "constructor", py::arg("value"))
        .def("get_next_sample", &rts::ConstantGenerator::get_next_sample)
        .def("reset", &rts::ConstantGenerator::reset);

    py::class_<rts::GaussianNoiseGenerator, rts::Generator, std::shared_ptr<rts::GaussianNoiseGenerator> >(m, "GaussianNoiseGenerator")
        .def(py::init<double, double>(), "constructor", py::arg("mean"), py::arg("stdDev"))
        .def("get_next_sample", &rts::GaussianNoiseGenerator::get_next_sample)
        .def("reset", &rts::GaussianNoiseGenerator::reset);

    py::class_<rts::RampGenerator, rts::Generator, std::shared_ptr<rts::RampGenerator> >(m, "RampGenerator")
        .def(py::init<double, double>(), "constructor", py::arg("startVal"), py::arg("step"))
        .def("get_next_sample", &rts::RampGenerator::get_next_sample)
        .def("reset", &rts::RampGenerator::reset);

    py::class_<rts::UniformRealGenerator, rts::Generator, std::shared_ptr<rts::UniformRealGenerator> >(m, "UniformRealGenerator")
        .def(py::init<double, double>(), "constructor", py::arg("startVal"), py::arg("step"))
        .def("get_next_sample", &rts::UniformRealGenerator::get_next_sample)
        .def("reset", &rts::UniformRealGenerator::reset);


}

