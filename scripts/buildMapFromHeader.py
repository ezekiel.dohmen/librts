#!/usr/bin/env python3
import os.path
import sys
import re

namespace = "rts::"

code_header = ("#ifndef LIGO_MODEL_VARS_LOOKUP_TABLE\n"
               "#define LIGO_MODEL_VARS_LOOKUP_TABLE\n"
               "\n"
               "#include \"ModelVar.hh\"\n"
               "#include \"controller.h\" //pLocalEpics\n"
               "\n\n"
              )

code_trailer = ("\n"
                "#endif //LIGO_MODEL_VARS_LOOKUP_TABLE\n"
                "\n"
               )

def typeToEnum(type):
    if type == "int":
        return namespace+"INT"
    elif type == "double":
        return namespace+"DOUBLE"
    elif type == "char":
        return namespace+"CHAR"

def convertName(name):
    #https://stackoverflow.com/questions/1175208/elegant-python-function-to-convert-camelcase-to-snake-case
    name = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    name = re.sub('__([A-Z])', r'_\1', name)
    name = re.sub('([a-z0-9])([A-Z])', r'\1_\2', name)
    return name.upper()

def checkDone(dimVals, curDimVals):
    for idx, n in enumerate(curDimVals):
        if n >= dimVals[idx]:
            return

def convertIndToStr(indices):
    indx_str = ""
    for index, n in enumerate(indices):
        indx_str += str(n)
        if index != len(indices)-1:
            indx_str += "_"
    return indx_str

def inc_indices(indices, max_indices):
    indices[-1] += 1

    if indices[0] >= max_indices[0] : #Catch the case where we have only one index
            return False

    for rinx in reversed(range(len(max_indices))):
        if indices[rinx] == max_indices[rinx]:
            indices[rinx] = 0
            indices[rinx-1] += 1

        if indices[0] >= max_indices[0] :
            return False
    return True

def flatten(max_indices):
    all_indices = []
    indices = [0]*len(max_indices)

    all_indices.append(convertIndToStr(indices))

    while inc_indices(indices, max_indices):
        all_indices.append(convertIndToStr(indices))

    return all_indices

def convertToCindices(indices_list):
    res = "["
    for idx, x in enumerate(indices_list):
        res += str(x) + "]"
        if idx != len(indices_list) -1:
            res += "["
    return res

def handel_array(line, structMember):
    result = []
    numDim = line.count("[")
    dimVals = []
    total = 1
    searchStartInd = 0

    for dim in range(numDim):
        s = line.find("[", searchStartInd)
        e = line.find("]", s+1)
        index = int(line[s+1:e])
        if index == 0: 
            print(f"Skipping variable {re.split(' |;', line)[1]}, because of 0 index size.")
            return []
        dimVals.append(index)
        total *= int(index)
        searchStartInd = e

    #print(f"Total calculated as {total}, dims {dimVals}")

    flattened_arr_postfix = flatten(dimVals)
    for suffix in flattened_arr_postfix:
        code_indices = suffix.split("_")
        res = re.split(' |;|\[', line)
        #print(code_indices)
        #print(f'{{ "{convertName(res[1])}_{suffix}", (void *)&(pLocalEpics->{structMember}.{res[1]}{convertToCindices(code_indices)}), {typeToEnum(res[0])}  }}')
        result.append(f'{{ "{convertName(res[1])}_{suffix}", (void *)&(pLocalEpics->{structMember}.{res[1]}{convertToCindices(code_indices)}), {typeToEnum(res[0])}  }}')
        
    return result

def buildModelInputsTable(lines, inStructName, structMember):

    result = []

    INPUT_START_VARS = f"typedef struct {inStructName} {{"
    INPUT_END_VARS = f"}} {inStructName};"

    in_model_struct_vars = False
    for line in lines:

        #print(f"Parsing line: {line}")

        if line.startswith("#define"):
            print(f"Skipping line : {line}")
        elif line == INPUT_START_VARS:
            in_model_struct_vars = True
        elif line == INPUT_END_VARS:
            in_model_struct_vars = False
        elif in_model_struct_vars and "[" in line:
            result += handel_array(line, structMember)
        elif in_model_struct_vars:
            #res = re.split(' |;', line)
            res = line.split()
            if len(res) >= 2:
                if res[0] == "unsigned":
                    TYPE_INDEX = 1
                    NAME_INDEX = 2
                else:
                    TYPE_INDEX = 0
                    NAME_INDEX = 1

                result.append(f'{{ "{convertName(res[NAME_INDEX][:-1])}", (void *)&(pLocalEpics->{structMember}.{res[NAME_INDEX][:-1]}), {typeToEnum(res[TYPE_INDEX])}  }}')
            else:
                pass #Skip what is a line without a type and name
    return result



def writeTable(lines, inStructName, structMember, outStructName, outputFile):

    outputFile.write(f"{namespace}model_var_lookup_t {outStructName}[] = {{\n")
    out_lines = buildModelInputsTable(lines, inStructName, structMember)
    for inx, line in enumerate(out_lines):
        outputFile.write(line)
        if inx != len(out_lines)-1:
            outputFile.write(",\n")

    outputFile.write("\n};\n\n");

def writeFilters(lines, outputFile):

    IN_FILTERS = False
    cur_filter = 0
    filters = []

    outputFile.write(f"{namespace}filter_module_lookup_t filter_modules[] = {{\n")

    for line in lines:
        if "SYSTEM_NAME_STRING_LOWER" in line:
            IN_FILTERS = True
        elif IN_FILTERS and "MAX_FIR" in line:
            IN_FILTERS = False
        elif IN_FILTERS and int(line.split()[2]) == cur_filter:
            filters.append((line.split()[1], line.split()[2]))
            cur_filter += 1
        elif IN_FILTERS == False:
            pass #No need for a warning print
        else:
            print(f"Skipping line in filter section: {line}")

    for inx, line in enumerate(filters):
        outputFile.write(f'{{ "{line[0]}", {line[1]} }}')
        if inx != len(filters)-1:
            outputFile.write(",\n")
        else:
            outputFile.write("\n};\n\n");

def writeEPICSModelFileInfo(filepath, outputFile):
    excitationPoints = []

    with open(filepath) as epics_file:
        lines = epics_file.readlines()

        for line in lines:
            if line.startswith("excitations "):
                excitationPoints = line.split()[1:]
            if line.startswith("test_points "):
                testPoints = line.split()[1:]

    #If we have test points 
    outputFile.write(f"{namespace}test_point_lookup_t test_points[] = {{\n")
    if len(testPoints) > 0:
        index = 0
        for name in testPoints:
            outputFile.write(f'{{ "{name}",  {index}}},\n');
            index += 1
    outputFile.write("};\n")

    #If we have excitation points
    outputFile.write(f"{namespace}excitation_point_lookup_t excitation_points[] = {{\n")
    if len(excitationPoints) > 0:
        index = 0
        for name in excitationPoints:
            outputFile.write(f'{{ "{name}",  {index}}},\n');
            index += 1
    outputFile.write("};\n")


def writeAllTables(model_header_filepath, model_epics_filepath, output_filepath):


    # The system name is the three chars after the IFO
    # epics/util/feCodeGen.pl:343
    system = os.path.basename(model_header_filepath)[2:5]

    with open(model_header_filepath) as file:
        lines = file.readlines()
        lines = [line.strip() for line in lines]

        with open(output_filepath, 'w') as wfile:

            #Write common header
            wfile.write(code_header)

            #Write common input table
            writeTable(lines, "CDS_EPICS_IN", "epicsInput", "common_inputs", wfile)

            #Write common output table
            writeTable(lines, "CDS_EPICS_OUT", "epicsOutput", "common_outputs", wfile)

            #Write model vars table
            writeTable(lines, system.upper(), system, "model_vars", wfile)

            #Write model filters table
            writeFilters(lines, wfile)

            #Write excitation points and test points
            writeEPICSModelFileInfo(model_epics_filepath, wfile)

            wfile.write(code_trailer)






if __name__ == "__main__":
    #buildTablesFromHeader(sys.argv[1])
    writeAllTables(sys.argv[1], sys.argv[2], sys.argv[3])


#    while inc_indices(indices, max_indices):
#        print(f"{indices}")    
#    for x in range (3):
#        print(f"{inc_indices(indices, max_indices)}, {indices}")


