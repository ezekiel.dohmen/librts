#include "LIGO_FilterModule.hh"

//libspdlog-dev
#include "spdlog/spdlog.h"


//advligorts
#include "fm10Gen_types.h"
#include "fm10Gen.h"
#include "controller.h"

using namespace rts;

//This is the every even bit in the 4-22 range
const unsigned LIGO_FilterModule::_stage_input_bits[] = {
    0x10,   0x40,    0x100,   0x400,    0x1000,
    0x4000, 0x10000, 0x40000, 0x100000, 0x400000 };

//This is every odd bit in the 5-23 range
const unsigned LIGO_FilterModule::_stage_output_bits[] = {
    0x20,   0x80,    0x200,   0x800,    0x2000,
    0x8000, 0x20000, 0x80000, 0x200000, 0x800000 };

LIGO_FilterModule& LIGO_FilterModule::enable_input()
{
    pDsp[ 0 ]->inputs[ _module_index ].opSwitchE |= OPSWITCH_INPUT_ENABLE;
    return *this;
}

LIGO_FilterModule& LIGO_FilterModule::disable_input()
{
    pDsp[ 0 ]->inputs[ _module_index ].opSwitchE &= ~OPSWITCH_INPUT_ENABLE;
    return *this;
}

LIGO_FilterModule& LIGO_FilterModule::enable_output()
{
    pDsp[ 0 ]->inputs[ _module_index ].opSwitchE |= OPSWITCH_OUTPUT_ENABLE;
    return *this;
}

LIGO_FilterModule& LIGO_FilterModule::disable_output()
{
    pDsp[ 0 ]->inputs[ _module_index ].opSwitchE &= ~OPSWITCH_OUTPUT_ENABLE;
    return *this;
}

LIGO_FilterModule& LIGO_FilterModule::enable_offset(double offset)
{
    pDsp[ 0 ]->inputs[ _module_index ].opSwitchE |= OPSWITCH_OFFSET_ENABLE;
    pDsp[ 0 ]->inputs[ _module_index ].offset = offset;
    return *this;
}

LIGO_FilterModule& LIGO_FilterModule::disable_offset()
{
    pDsp[ 0 ]->inputs[ _module_index ].opSwitchE &= ~OPSWITCH_OFFSET_ENABLE;
    return *this;
}

LIGO_FilterModule& LIGO_FilterModule::enable_limiter(double absLimit)
{
    pDsp[ 0 ]->inputs[ _module_index ].opSwitchE |= OPSWITCH_LIMITER_ENABLE;
    pDsp[ 0 ]->inputs[ _module_index ].limiter = absLimit;
    return *this;
}

LIGO_FilterModule& LIGO_FilterModule::disable_limiter()
{
    pDsp[ 0 ]->inputs[ _module_index ].opSwitchE &= ~OPSWITCH_LIMITER_ENABLE;
    return *this;
}

LIGO_FilterModule& LIGO_FilterModule::enable_hold()
{
    pDsp[ 0 ]->inputs[ _module_index ].opSwitchE |= OPSWITCH_HOLD_ENABLE;
    return *this;
}

LIGO_FilterModule& LIGO_FilterModule::disable_hold()
{
    pDsp[ 0 ]->inputs[ _module_index ].opSwitchE &= ~OPSWITCH_HOLD_ENABLE;
    return *this;
}

LIGO_FilterModule& LIGO_FilterModule::set_gain(double gain)
{
    pDsp[ 0 ]->inputs[ _module_index ].outgain = gain;
    return *this;
}

LIGO_FilterModule& LIGO_FilterModule::set_ramp_time_s(double time_s)
{
    pDsp[ 0 ]->inputs[ _module_index ].gain_ramp_time = time_s;
    return *this;
}

LIGO_FilterModule& LIGO_FilterModule::enable_stages( const std::vector<int> & indices)
{
    for (const auto& index : indices)
    {
        if(index >= 0 && index < FILTERS)
            pDsp[ 0 ]->inputs[ _module_index ].opSwitchE |= _stage_input_bits[index];
        else
        {
            spdlog::warn("LIGO_FilterModule::enableStages() - Filter stage index {} skipped because it is out of the acceptable range [0-{}]",\
                         index,
                         0,
                         FILTERS-1);
        }
    }
    return *this;
}

LIGO_FilterModule& LIGO_FilterModule::disable_stages( const std::vector<int> & indices)
{
    for (const auto& index : indices)
    {
        if(index >= 0 && index < FILTERS)
            pDsp[ 0 ]->inputs[ _module_index ].opSwitchE &= ~_stage_input_bits[index];
        else
        {
            spdlog::warn("LIGO_FilterModule::disableStages() - Filter stage index {} skipped because it is out of the acceptable range [0-{}]",\
                         index,
                         0,
                         FILTERS-1);
        }
    }
    return *this;
}


