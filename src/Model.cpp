#include "Model.hh"
#include "LIGO_FilterModule.hh"
#include "ModelVar.hh"

#include "GaussianNoiseGenerator.hh"
#include "CannedSignalGenerator.hh"
#include "ConstantGenerator.hh"

//libspdlog-dev
#include "spdlog/spdlog.h"
#include "spdlog/cfg/env.h"

//module build
#include "var_lookup_table.h"

//advligorts
#include "modelRateInfo.h"
#include "fm10Gen.h" // checkFiltReset,
#include "../fe/mapApp.h" //initmap, mapPciModules
#include "../fe/print_io_info.h" //print_io_info()
#include "../fe/rcguserCommon.h" //attach_shared_memory()
#include "feComms.h" //RFM_FE_COMMS
#include "controller.h"

#include <map>

using namespace rts;

//
// The RCG generated code does not define this at all in the 
// case where no IPCs are present in the model, that leads to 
// undefined refrencens, so we define it in that case
#if MODEL_NUM_IPCS_USED == 0
CDS_IPC_INFO ipcInfo[1];
#endif

class Model::Impl
{
    public:

        static std::unique_ptr<Model::Impl> create_instance();
        virtual ~Impl(); 

        int get_model_rate_Hz();
        int get_num_filter_modules();
        std::vector<std::string> get_all_input_names();
        std::vector<std::string> get_all_adc_names();
        std::vector<std::string> get_all_dac_names();
        std::vector<std::string> get_all_filter_names();

        std::vector<std::string> get_all_model_var_names();

        bool set_var(const std::string& name, double val);
        bool set_var(const std::string& name, int val);

        bool set_var_momentary(const std::string& name, double val);

        template< class T>
        std::optional<T>  get_var(const std::string& name) const;

        bool record_model_var(const std::string & name, int record_rate_hz);
        bool stop_recording_model_var(const std::string & name);

        
        template< class T>
        std::optional<std::vector<T>> get_recorded_var(const std::string & name) const;

        std::optional<LIGO_FilterModule> get_filter_module_by_name(std::string name);
        std::optional<LIGO_FilterModule> get_filter_module_by_id(int module_index);
        bool reset_filter_module(const std::string & name);
        bool load_biquad_coefficients(const std::string & filter_name,
                                      const std::vector<int> & stageIndicesToLoad,
                                      const std::vector<double> & coefficients);
        bool load_scipy_sos_coef(const std::string & filter_name,
                                 const std::vector<int> & stageIndicesToLoad,
                                 const std::vector< std::vector<double> > & sections_and_coefficients);



        void set_default_inputs();
        void set_adc_channel_generator(int adcNum, int chanNum, std::shared_ptr<Generator> gen);
        bool set_excitation_point_generator(const std::string & name, std::shared_ptr<Generator> gen);
        bool set_ipc_receiver_generator(const std::string & name, std::shared_ptr<Generator> gen);
        void record_dac_output(bool shouldRec);
        const std::vector< double > & get_dac_output_by_id(int dacIndex, int chanIndex);




        int run_model(unsigned num_cycles);

    private:

        bool _isIOP;
        int _numPCIeCards;

        //Pointer to EPICS shared memory space
        RFM_FE_COMMS* _pEpicsComms;

        //Start filter data
        /// Pointer to filter coeffs local memory.
        COEF _dspCoeff[ NUM_SYSTEMS ]; // Local mem for SFM coeffs.
        //TODO: Should this be in shared memory?


        //Start Operatinal Variables
        int _cycleNum = 0;

        //A [cards][chans] multidimensional vector, storing all channel generators
        std::vector< std::vector< std::shared_ptr<Generator> > > _adcs;

        bool _recordDacOutput = true;
        std::vector< std::vector< std::vector< double > > > _dacOutputs;

        std::map< std::string, ModelVarLocation > _model_vars;
        std::map< std::string, int > _filter_modules;

        typedef struct excitation_record_t {
            ModelVarLocation target_var;
            std::shared_ptr<Generator> exc_generator;
        } excitation_record_t;
        std::map< std::string, excitation_record_t > _excitation_points;
        
        
        struct ipc_data_gen_map_t { int ipc_index; int generator_index; };
        std::map< std::string, ipc_data_gen_map_t > _ipc_receivers;
        std::vector< std::shared_ptr<Generator> > _ipc_generators;

        std::map< std::string, ModelVarValue > _momentary_vars;
        

        //
        // Start runtime members
        typedef struct recored_var_info_t {
            int samples_per_record;
            ModelVarLocation var_info;
            std::vector< ModelVarValue > values;
        } recored_var_info_t;
        std::map< std::string, recored_var_info_t > _vars_to_record; //This is a list of currently recorded model vars

        //
        // Start Private Member Functions
        //
        Impl() {};
        void clear_dac_outputs();
        bool init_filter_modules();
        bool initialize_model_globals();

        template< class T>
        bool setVarTemplate(const std::string& name, T val);
        void setValueFromLocation(ModelVarValue & set_me, ModelVarLocation & with_me);



        std::vector<double> scipy_sos_to_adv_sos(const std::vector< std::vector<double> > & scipy_coeffs);
        std::vector<double> adv_df_sos_to_adv_biquad(const std::vector< double > coeffs);



};

std::unique_ptr<Model::Impl> Model::Impl::create_instance()
{
    std::unique_ptr<Model::Impl> me_ptr (new Model::Impl());

    if( !me_ptr->initialize_model_globals() ) return nullptr;

    //TODO: the generated var_lookup_table.h only has offsets because pLocalEpics is set on initialization
    //Maybe we can find a better way than this
    char* base = (char*)pLocalEpics;

    //Add common inputs
    for (const model_var_lookup_t & var : common_inputs)
    {
        spdlog::debug("Adding {} to known model vars, type: {}", var.var_name, SUPPORTED_TYPES_NAMES[var.type]);
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >( 
                                               std::string(var.var_name), ModelVarLocation( (uint64_t)var.val_ptr + base, var.type) ) );
    }

    //Add common outputs
    for (const model_var_lookup_t & var : common_outputs)
    {
        spdlog::debug("Adding {} to known model vars, type: {}", var.var_name, SUPPORTED_TYPES_NAMES[var.type]);
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                               std::string(var.var_name), ModelVarLocation( (uint64_t)var.val_ptr + base, var.type) ) );
    }

    //Add model variables
    for (const model_var_lookup_t & var : model_vars)
    {
        spdlog::debug("Adding {} to known model vars, type: {}", var.var_name, SUPPORTED_TYPES_NAMES[var.type]);
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                               std::string(var.var_name), ModelVarLocation( (uint64_t)var.val_ptr + base, var.type) ) );
    }

    //Add test points to model vars
    for (const auto & var : test_points)
    {
        spdlog::debug("Adding {} testpoint to known model vars, type: {}", var.name, SUPPORTED_TYPES_NAMES[rts::DOUBLE]);
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                           std::string(var.name), ModelVarLocation( testpoint[var.index], rts::DOUBLE) ));
    }

    //Each filter module has three test points in it, so we find and add them here
    for (const filter_module_lookup_t & var : filter_modules)
    {
        spdlog::debug("Adding {}(_IN1, _IN2, _OUT) to known filter testpoints.", var.filter_name);
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                        std::string(var.filter_name) + std::string("_OUT"), 
                                        ModelVarLocation( &dspPtr[ 0 ]->data[ var.module_index ].testpoint, rts::DOUBLE) ));
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                        std::string(var.filter_name) + std::string("_IN1"), 
                                        ModelVarLocation( &dspPtr[ 0 ]->data[ var.module_index ].filterInput, rts::DOUBLE) ));
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                        std::string(var.filter_name) + std::string("_IN2"), 
                                        ModelVarLocation( &dspPtr[ 0 ]->data[ var.module_index ].inputTestpoint, rts::DOUBLE) ));
    }


    //Add filter modules to filter map
    for (const filter_module_lookup_t & var : filter_modules)
    {
        spdlog::debug("Adding {} to known filter modules.", var.filter_name);
        me_ptr->_filter_modules.insert( std::pair< std::string, int >(
                                        std::string(var.filter_name), var.module_index ) );
    }


    //Add default excitation points
    int exc_index = 0;
    for (const auto & var : excitation_points)
    {
        spdlog::debug("Adding {} to known excitation points", var.name);
        me_ptr->_excitation_points.insert( std::pair< std::string, excitation_record_t >(
                                           std::string(var.name), 
                                           {
                                               ModelVarLocation( &xExc[exc_index], rts::DOUBLE),
                                               std::make_shared< ConstantGenerator >(0)
                                           }
                                           ) );
        ++exc_index;
    }

    //Add excitations from filter modules
    for (const filter_module_lookup_t & var : filter_modules)
    {
        spdlog::debug("Adding {}_EXC to known filter excitation points.", var.filter_name);
        me_ptr->_excitation_points.insert( std::pair< std::string, excitation_record_t >(
                                           std::string(var.filter_name) + "_EXC", 
                                           {
                                               ModelVarLocation( &dspPtr[ 0 ]->data[ var.module_index ].exciteInput, rts::DOUBLE),
                                               std::make_shared< ConstantGenerator >(0)
                                           }
                                           ) );
    }

    //Add IPC senders to model vars, so we can trend them
    for (int i = 0; i < MODEL_NUM_IPCS_USED; ++i)
    {
        if( ipcInfo[i].mode == ISND)
        {
            spdlog::debug("Adding {} IPC sender to known model vars", ipcInfo[i].name);
            me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                           std::string(ipcInfo[i].name), ModelVarLocation( &ipcInfo[i].data, rts::DOUBLE) ));
        }
    }


    //Find receivers that don't have senders in the same model
    std::map<int, int> ipcs_map; //<ipc num, index>
    std::map<int, int>::iterator it;
    for (int i = 0; i < MODEL_NUM_IPCS_USED; ++i)
    {
        it = ipcs_map.find(ipcInfo[i].ipcNum);
        if (it != ipcs_map.end())
            ipcs_map.erase(it); //We found it again so we have both sender and receiver, remove it
        else
            ipcs_map[ipcInfo[i].ipcNum] = i; //If not found, we add to map
    }

    //After the above loop the ipcs_map only has IPCs that don't have a sender and receiver in this model
    int gen_index = 0;
    for (auto const& [ipcNum, ipcIndex] : ipcs_map)
    {
        if(ipcInfo[ipcIndex].mode == IRCV)
        {
            spdlog::debug("Adding {} to known simulated IPC receivers", ipcInfo[ipcIndex].name);
            me_ptr->_ipc_receivers.insert( std::pair< std::string, Model::Impl::ipc_data_gen_map_t >(
                                           std::string(ipcInfo[ipcIndex].name), {ipcIndex, gen_index } ) );
            ++gen_index;
        }
    }


    //Consant 0s input to all ADCs and excitation points
    me_ptr->set_default_inputs();

    return me_ptr;

}

Model::Impl::~Impl()
{
    detach_shared_memory();
}


int Model::Impl::get_model_rate_Hz()
{
    return FE_RATE; //Global from <modelName>.h
}

int Model::Impl::get_num_filter_modules()
{
    return MAX_MODULES; //Global from <modelName>.h
}

void Model::Impl::setValueFromLocation(ModelVarValue & set_me, ModelVarLocation & with_me)
{
     switch ( with_me.get_type() )
     {
        case INT:
            set_me.value.asInt = *( static_cast<int*>( with_me.get_ptr() ) );
            break;
        case DOUBLE:
            set_me.value.asDouble = *( static_cast<double*>( with_me.get_ptr() ) );
            break;
        case CHAR:
            set_me.value.asChar = *( static_cast<char*>( with_me.get_ptr() ) );
            break;
        default:
            break;
     }
}


template< class T>
bool Model::Impl::setVarTemplate(const std::string& name, T val)
{
    auto it = _model_vars.find(name);
    if( it == _model_vars.end()) 
    {
        spdlog::error("setVarTemplate() - The var with the name {} could not be found.\n", name);
        return false;
    }

    switch ( it->second.get_type() )
    {
        case INT:
            *( static_cast<int*>( it->second.get_ptr() ) ) = static_cast<int>(val);
            break;
        case DOUBLE:
            *( static_cast<double*>( it->second.get_ptr() ) ) = static_cast<double>(val);
            break;
        case CHAR:
            *( static_cast<char*>( it->second.get_ptr() ) ) = static_cast<char>(val);
            break;
        default:
            spdlog::error("setVarTemplate() - Hitting the default case with var name {}.\n", name);
            return false;
    }
    return true;


}

bool Model::Impl::set_var(const std::string& name, double val)
{
    return setVarTemplate<double>(name, val);
}

bool Model::Impl::set_var(const std::string& name, int val)
{
    return setVarTemplate<int>(name, val);
}

bool Model::Impl::set_var_momentary(const std::string& name, double val)
{
    auto it = _model_vars.find(name);
    if( it == _model_vars.end()) 
    {
        spdlog::error("set_var_momentary() - The var with the name {} could not be found.", name);
        return false;
    }    
    ModelVarValue stored_val;
    stored_val.value.asDouble = val;
    _momentary_vars.insert( std::pair<std::string, ModelVarValue>(name, stored_val) );
    return true;
}


template< class T>
std::optional<T>  Model::Impl::get_var(const std::string& name) const
{
    auto it = _model_vars.find(name);
    if( it == _model_vars.end())
    {
        spdlog::error("get_var() - The var with the name {} could not be found.", name);
        return {};
    }

    return it->second.get_val<T>();
}

bool Model::Impl::record_model_var(const std::string & name, int record_rate_hz)
{
    auto it = _model_vars.find(name);
    if( it == _model_vars.end())
    {
        spdlog::error("record_model_var() - The var with the name {} could not be found.", name);
        return false;
    }

    if(record_rate_hz > get_model_rate_Hz() )
    {
        spdlog::warn("record_model_var() - Requested record rate for {} was {}, but model only runs at {}. Using model rate.", name, record_rate_hz, get_model_rate_Hz());
        record_rate_hz = get_model_rate_Hz();
    }

    int cycles_per_record =  std::ceil(((double)get_model_rate_Hz() / record_rate_hz));

    auto fit = _vars_to_record.find(name); 
    if(fit != _vars_to_record.end())
    {
         //We are already recording the requested var, update the rate
         fit->second.samples_per_record = cycles_per_record;
    }
    else
    {
        _vars_to_record.insert( std::pair< std::string, recored_var_info_t > (name, {cycles_per_record, it->second, {}}));
    }
    
    return true;
}

bool Model::Impl::stop_recording_model_var(const std::string & name)
{
    auto it = _model_vars.find(name);
    if( it == _model_vars.end())
    {
        spdlog::warn("stop_recording_model_var() - The var with the name {} could not be found.", name);
        return false;
    }

    //Check if we are recording a var with the name
    auto fit = _vars_to_record.find(name);
    if ( fit == _vars_to_record.end() )
    {
        spdlog::warn("stop_recording_model_var() - The var with the name {} could not be found in the recording list.", name);
        return false;
    }

    _vars_to_record.erase(fit);

    return true;

}


template< class T>
std::optional<std::vector<T>> Model::Impl::get_recorded_var(const std::string & name) const
{
    auto fit = _vars_to_record.find(name);
    if ( fit == _vars_to_record.end() )
    {
        spdlog::error("get_recorded_var() - The var with the name {} could not be found in the recording list.", name);
        return {};
    }

    std::vector<T> trend;
    trend.resize(fit->second.values.size());


    for( unsigned i=0; i<fit->second.values.size(); ++i )
    {
        trend[i] = fit->second.values[i].get_val<T>(fit->second.var_info.get_type());
    }

    return trend;
}


std::optional<LIGO_FilterModule> Model::Impl::get_filter_module_by_name(std::string name)
{
    auto it = _filter_modules.find(name);
    if ( it == _filter_modules.end())
        return {};

    return LIGO_FilterModule(it->second);
}

std::optional<LIGO_FilterModule> Model::Impl::get_filter_module_by_id(int module_index)
{
    if(module_index < 0 || module_index >= get_num_filter_modules())
        return {};
    return LIGO_FilterModule(module_index);
}

bool Model::Impl::reset_filter_module(const std::string & name)
{
    auto it = _filter_modules.find(name);
    if ( it == _filter_modules.end())
    {
        spdlog::error("reset_filter_module() - Could not find a filter with the name {}", name);
        return false;
    }

    int module_index = it->second;
 
    //From epics/util/skeleton.st
    //rset of 1 is signalling new coeff flags
    //rset of 2 resets the history
    pDsp [ 0 ]->inputs[module_index].rset = 2;

    checkFiltReset( module_index,
                    dspPtr[ 0 ],
                    pDsp [ 0 ],
                    &_dspCoeff[ 0 ],
                    get_num_filter_modules(),
                    pCoeff[ 0 ]);
    spdlog::info("reset_filter_module() - rset: {}", pDsp [ 0 ]->inputs[module_index].rset);

    return true;
}

bool Model::Impl::load_biquad_coefficients(const std::string & filter_name, 
                                         const std::vector<int> & stageIndicesToLoad, 
                                         const std::vector<double> & coefficients)
{

    if( (coefficients.size()-1) % 4 != 0)
    {
        spdlog::error("load_biquad_coefficients() - Len of coefficients-1({}) is not a multiple of 4.", coefficients.size()-1);
        return false;
    }

    auto it = _filter_modules.find(filter_name);
    if ( it == _filter_modules.end())
    {
        spdlog::error("load_biquad_coefficients() - Could not find a filter with the name {}", filter_name);
        return false;
    }

    for(const auto& sfi : stageIndicesToLoad)
    {
        if(sfi >= 0 && sfi < FILTERS)
        {
            for(unsigned cof_index = 0; cof_index < coefficients.size(); ++cof_index)
                _dspCoeff[ 0 ].coeffs[ it->second ].filtCoeff[sfi][cof_index] = coefficients[cof_index];
            _dspCoeff[ 0 ].coeffs[ it->second ].filtSections[sfi] = coefficients.size() - 1; //We don't count the gain value
            _dspCoeff[ 0 ].coeffs[ it->second ].biquad = 1; //Because we are setting BiquadCoefficients we set to biquad form
        }
        else
        {
            spdlog::warn("load_biquad_coefficients() - Sub filter index {} skipped because it is out of the acceptable range [0-{}]",\
                         sfi,
                         0,
                         FILTERS-1);
        }
    }
    return true;
}

bool Model::Impl::load_scipy_sos_coef(const std::string & filter_name,
                                  const std::vector<int> & stageIndicesToLoad,
                                  const std::vector< std::vector<double> > & sections_and_coefficients)
{
    auto it = _filter_modules.find(filter_name);
    if ( it == _filter_modules.end())
    {
        spdlog::error("load_scipy_sos_coef() - Could not find a filter with the name {}", filter_name);
        return false;
    }

    std::vector<double> adv_sos_coeffs = scipy_sos_to_adv_sos(sections_and_coefficients);
    if(adv_sos_coeffs.size() == 0)
    {
        spdlog::error("load_scipy_sos_coef() - Could not convert the given sections and coefficients to advLIGO format.");
        return false;
    }

    //Check if the filter we are trying to load is biquad, and do conversion
    if(_dspCoeff[ 0 ].coeffs[it->second].biquad == 1)
    {
        const auto & biquad_sos_coef = adv_df_sos_to_adv_biquad(adv_sos_coeffs);
        return load_biquad_coefficients(filter_name, stageIndicesToLoad, biquad_sos_coef);
    }
    else
    {
        spdlog::error("load_scipy_sos_coef() - Error unsupported filter type.");
        return false;
    }


    return true;
}

std::vector<double> Model::Impl::scipy_sos_to_adv_sos(const std::vector< std::vector<double> > & scipy_sections_and_coeffs)
{
    /* The scipy format is documented here:
     * https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.sosfilt.html
     *
     * It is in the format of a list of lists with dimensions [N][6]
     * Where N is the number of second order sections (SOSs)
     * And the 6 elements are a SOS with the first three elements as the
     * numerator coefficients and the last three the denominator coefficients
     *
     * sp = scipy_coeffs[0]
     * H(z) = (sp[0] + sp[1] + sp[2]) / (sp[3] + sp[4] +sp[5])
     *
     * The advligo format is [G, a_11, a_21, b_11, b_21, a_12, a_22, b_12, b_22, ... ]
     *
     */

    std::vector<double> flattened_adv_sos_format;
    flattened_adv_sos_format.resize(1);
    flattened_adv_sos_format[0] = 1.0; //Start gain factor at 1


    for(const auto& sos : scipy_sections_and_coeffs)
    {
        if (sos.size() != 6)
        {
            spdlog::error("scipy_sos_to_adv_sos() - coefficients length was not 6 as expected");
            return {}; 
        }
        flattened_adv_sos_format[0] *= sos[0]/sos[3];

        std::vector<double> temp{sos[4]/sos[3], sos[5]/sos[3], sos[1]/sos[0], sos[2]/sos[0]};

        flattened_adv_sos_format.insert(flattened_adv_sos_format.end(),
                                        temp.begin(),
                                        temp.end());
    }
    return flattened_adv_sos_format;
}

std::vector<double> Model::Impl::adv_df_sos_to_adv_biquad(const std::vector< double > coeffs)
{
    /*Returns advligorts SOS coefficients in direct form converted to biquad format.

    The biquad representation reduces quantization noise by avoiding
    large internal values. The relationship of its coefficients to those
    of the direct form II filter is given in:
    https://dcc.ligo.org/LIGO-G0900928
    */
    // reference implementation:
    // https://git.ligo.org/cds/advligorts/-/blob/master/src/drv/fmReadCoeff.c

    std::vector< double > result(coeffs.size());
    result[0] = coeffs[0]; 
    for (unsigned n=1; n < result.size(); n+=4)
    {
        /*
        a1, a2, b1, b2 = coef[n], coef[n+1], coef[n+2], coef[n+3]
        out[n] = -a1 - 1
        out[n+1] = -a2 - a1 - 1
        out[n+2] = b1 - a1
        out[n+3] = b2 - a2 + b1 - a1*/
        result[n] = -coeffs[n] - 1.0;
        result[n+1] = -coeffs[n+1] - coeffs[n] - 1.0;
        result[n+2] = coeffs[n+2] - coeffs[n];
        result[n+3] = coeffs[n+3] - coeffs[n+1] + coeffs[n+2] - coeffs[n];
    }
    return result;

}

std::vector<std::string> Model::Impl::get_all_filter_names()
{
    std::vector<std::string> all_filter_names;
    all_filter_names.reserve(_filter_modules.size());
    for (auto const & pair: _filter_modules) {
        all_filter_names.push_back(pair.first);
    }
    return all_filter_names;
}

std::vector<std::string> Model::Impl::get_all_adc_names()
{
    std::vector<std::string> all_adc_chans;
    for (int cur_card =0; cur_card < cdsPciModules.adcCount; ++cur_card)
    {
        for(int cur_chan=0; cur_chan < MAX_ADC_CHN_PER_MOD; ++cur_chan)
        {
            if(dWordUsed[cur_card][cur_chan] )
            {
               all_adc_chans.push_back("adc_" + std::to_string(cur_card) + "_" + std::to_string(cur_chan));
            }
        }
    }
    return all_adc_chans;
}

std::vector<std::string> Model::Impl::get_all_dac_names()
{
    std::vector<std::string> all_dac_chans;
    for (int cur_card =0; cur_card < cdsPciModules.dacCount; ++cur_card)
    {
        for(int cur_chan=0; cur_chan < MAX_DAC_CHN_PER_MOD; ++cur_chan)
        {
            if(dWordUsed[cur_card][cur_chan] )
            {
               all_dac_chans.push_back("dac_" + std::to_string(cur_card) + "_" + std::to_string(cur_chan));
            }
        }
    }
    return all_dac_chans;
}


std::vector<std::string> Model::Impl::get_all_input_names()
{
    std::vector<std::string> inputNames = get_all_adc_names();
    std::vector<std::string> dac = get_all_dac_names();

      inputNames.insert(
      inputNames.end(),
      std::make_move_iterator(dac.begin()),
      std::make_move_iterator(dac.end())
    );

    return inputNames;
}

std::vector<std::string> Model::Impl::get_all_model_var_names()
{
    std::vector<std::string> names;
    for (auto const& [key, val] : _model_vars)
    {
        names.push_back(key);
    } 
    return names;
}

void Model::Impl::set_default_inputs()
{
    //Set default ADC inputs
    _adcs.resize(cdsPciModules.adcCount);
    for (int cur_card =0; cur_card < cdsPciModules.adcCount; ++cur_card)
    {
        for(int cur_chan=0; cur_chan < MAX_ADC_CHN_PER_MOD; ++cur_chan)
        {
           _adcs[cur_card].push_back( std::make_shared< ConstantGenerator >(0) );
        }
    }

    
    //Set default ipc receiver inputs
    _ipc_generators.resize( _ipc_receivers.size() );
    for( unsigned i = 0; i < _ipc_receivers.size(); ++i )
    {
        _ipc_generators[i] = std::make_shared< ConstantGenerator >(0);
    }

}

void Model::Impl::set_adc_channel_generator(int adcNum, int chanNum, std::shared_ptr<Generator> gen)
{
    _adcs.at(adcNum).at(chanNum) = gen;
}

bool Model::Impl::set_excitation_point_generator(const std::string & name, std::shared_ptr<Generator> gen)
{
    auto it = _excitation_points.find(name);
    if(it == _excitation_points.end())
    {
        spdlog::error("set_excitation_point_generator() - Could not find an excitation point with the name {}", name);
        return false;
    }
    it->second.exc_generator = gen;
    return true;
}


bool Model::Impl::set_ipc_receiver_generator(const std::string & name, std::shared_ptr<Generator> gen)
{
    auto it = _ipc_receivers.find(name);
    if(it == _ipc_receivers.end())
    {
        spdlog::error("set_ipc_point_generator() - Could not find an ipc receiver with the name {}", name);
        return false;
    }
    _ipc_generators[it->second.generator_index] = gen;
    return true;
}

void Model::Impl::record_dac_output(bool shouldRec)
{
    _recordDacOutput = shouldRec;
}

const std::vector< double > & Model::Impl::get_dac_output_by_id(int dacIndex, int chanIndex)
{
    return _dacOutputs.at(dacIndex).at(chanIndex);
}

int  Model::Impl::run_model(unsigned num_cycles)
{

    double cycle_adc_input[MAX_ADC_MODULES][MAX_ADC_CHN_PER_MOD];
    double cycle_dac_output[MAX_DAC_MODULES][MAX_DAC_CHN_PER_MOD];

    for (auto const& [name, var] : _momentary_vars )
    {
        //Save old value, set momentary, save old value
        ModelVarValue hold;
        hold.value.asDouble = get_var<double>( name ).value();
        set_var(name, var.get_val<double>( rts::SUPPORTED_TYPES::DOUBLE ));
        _momentary_vars[name] = hold;
    }


    if( _recordDacOutput )
    {
        //Allocate space for DAC outputs is we are collecting them
        _dacOutputs.resize(cdsPciModules.dacCount);
        for (int cur_card =0; cur_card < cdsPciModules.dacCount; ++cur_card)
        {
            _dacOutputs[cur_card].resize(MAX_DAC_CHN_PER_MOD);
            for(int cur_chan=0; cur_chan < MAX_DAC_CHN_PER_MOD; ++cur_chan)
                _dacOutputs[cur_card][cur_chan].resize(num_cycles);
        }
    }
    else
    {
        //We don't want to have old samples returned when _recordDacOutput == false
        for (unsigned cur_card =0; cur_card < _dacOutputs.size(); ++cur_card)
        {
            for(int cur_chan=0; cur_chan < MAX_DAC_CHN_PER_MOD; ++cur_chan)
                _dacOutputs[cur_card][cur_chan].clear();
        }
    }

    // Set up space for any model vars we are recording
    for (auto & [key, val] : _vars_to_record)
    {
        val.values.reserve(num_cycles);
    }


    //Run model for the requested number of cycles
    for(unsigned i=0; i<num_cycles; ++i)
    {

        //Generate samples from ADCs
        for (int cur_card =0; cur_card < cdsPciModules.adcCount; ++cur_card)
        {
            for(int cur_chan=0; cur_chan < MAX_ADC_CHN_PER_MOD; ++cur_chan)
            {
               cycle_adc_input[cur_card][cur_chan] = _adcs[cur_card][cur_chan]->get_next_sample();
            }
        }

        //Generate samples from excitations
        for (auto & [key, val] : _excitation_points)
        {
            val.target_var.set_val( val.exc_generator->get_next_sample() );
        }

        
        //Generate samples from ipc receivers with no senders in this model
        int rcvBlock = ( ( _cycleNum ) * ( IPC_MAX_RATE / IPC_RATE ) ) % IPC_BLOCKS;
        timeSec = 0;
        int cycle65k = 0;
        for (auto const& x : _ipc_receivers)
        {
            cycle65k = ( ( _cycleNum * ipcInfo[ x.second.ipc_index ].sendCycle ) );

            ipcInfo[ x.second.ipc_index ].pIpcDataRead[ 0 ]
                                   ->dBlock[ rcvBlock ][ x.second.ipc_index ]
                                   .timestamp = cycle65k;

            ipcInfo[ x.second.ipc_index ].pIpcDataRead[ 0 ]
                              ->dBlock[ rcvBlock ][ x.second.ipc_index ]
                              .data = _ipc_generators[x.second.generator_index].get()->get_next_sample();
        }

        feCode( _cycleNum,
                cycle_adc_input, //double[ADC NUM][Chan Num]
                cycle_dac_output, //double[DAC Num][Chan Num]
                dspPtr[ 0 ], //Global from controller.h, ptr to EPICS shmem dspSpace
                &_dspCoeff[ 0 ], //Global in rtcds system, but we make it local
                (struct CDS_EPICS*)pLocalEpics,
                0 );

        if( _recordDacOutput )
        {
            for (int cur_card =0; cur_card < cdsPciModules.dacCount; ++cur_card) {
                for(int cur_chan=0; cur_chan < MAX_DAC_CHN_PER_MOD; ++cur_chan) {
                _dacOutputs[cur_card][cur_chan][i] = cycle_dac_output[cur_card][cur_chan];
                }
            }
        }

        for (auto & [key, val] : _vars_to_record)
        {
            if( _cycleNum % val.samples_per_record  == 0)
            {
                val.values.push_back( {} );
                setValueFromLocation( val.values.back(), val.var_info );
            }
        }

        //Clear any momentary vars we just set, 
        //we do this after dac and variable recording for the cycle
        for (const auto& [name, val]: _momentary_vars)
        {
            set_var(name, val.get_val<double>(rts::SUPPORTED_TYPES::DOUBLE)); //Reset the old value
        }
        _momentary_vars.clear();

        ++_cycleNum;
        _cycleNum %= CYCLE_PER_SECOND;
    }





    //TODO: if we ever have alarms that stop model 
    //      simulation this could be less than requested
    return num_cycles;
}


//
// Start Private Impl Member Functions
//

void Model::Impl::clear_dac_outputs()
{
    for ( int ii = 0; ii < MAX_DAC_MODULES; ii++ )
    {
        for ( int jj = 0; jj < MAX_DAC_CHN_PER_MOD; jj++ )
        {
            dacOut[ ii ][ jj ] = 0.0;
            dacOutUsed[ ii ][ jj ] = 0;
        }
    }
}

bool Model::Impl::init_filter_modules( )
{
    int system, ii, jj, kk;

    /// \> Init IIR filter banks
    //   Initialize filter banks  *********************************************
    //We have all these loops over NUM_SYSTEMS but that might always be 1?
    for ( system = 0; system < NUM_SYSTEMS; system++ )
    {
        for ( ii = 0; ii < MAX_MODULES; ii++ )
        {
            for ( jj = 0; jj < FILTERS; jj++ )
            {
                for ( kk = 0; kk < MAX_COEFFS; kk++ )
                    _dspCoeff[ system ].coeffs[ ii ].filtCoeff[ jj ][ kk ] = 0.0;
                for( kk = 0; kk < MAX_HISTRY; ++kk)
                    _dspCoeff[ system ].coeffs[ ii ].filtHist[ jj ][ kk ] = 0.0;
                _dspCoeff[ system ].coeffs[ ii ].filtSections[ jj ] = 0;
            }
        }
    }

    // Initialize all filter module excitation signals to zero
    // and gains to 0, this means everything ramps from 0 if you have
    // a non-zero ramp time on your module
    for ( ii = 0; ii < MAX_MODULES; ii++ )
    {
        std::memset(&pDsp[ 0 ]->data[ ii ], 0, sizeof(pDsp[ 0 ]->data[ ii ]));
        pDsp[ 0 ]->inputs[ ii ].offset = 0;
        pDsp[ 0 ]->inputs[ ii ].outgain = 0;
        pDsp[ 0 ]->inputs[ ii ].gain_ramp_time = 0;
        pDsp[ 0 ]->inputs[ ii ].limiter = 0;
        for ( jj = 0; jj < FILTERS; jj++ )
        {
            pDsp[ 0 ]->inputs[ ii ].rmpcmp[jj] = 0;
            pDsp[ 0 ]->inputs[ ii ].cnt[jj] = 0;
            pDsp[ 0 ]->inputs[ ii ].timeout[jj] = 0;
        }
    }

    /// \> Initialize IIR filter bank values
    if ( initVars( pDsp[ 0 ], pDsp[ 0 ], _dspCoeff, MAX_MODULES, pCoeff[ 0 ] ) )
    {
        spdlog::critical( "init_filter_modules() - Filter module init failed, exiting\n" );
        return false;
    }

    //Set up filters for output
    for(ii=0; ii<MAX_MODULES; ++ii)
    {
        _dspCoeff[ 0 ].coeffs[ii].biquad = 1;
        for ( jj = 0; jj < FILTERS; jj++ )
            _dspCoeff[ 0 ].coeffs[ii].sType[jj] = 21;
    }
   
   /* 
    for(ii=0; ii<MAX_MODULES; ++ii)
    {
        pDsp[ 0 ]->inputs[ ii ].outgain = 1;
        pDsp[ 0 ]->inputs[ ii ].gain_ramp_time = 1.0; //1s
        pDsp[ 0 ]->inputs[ ii ].limiter = 0;
        pDsp[ 0 ]->inputs[ ii ].opSwitchE = OPSWITCH_INPUT_ENABLE | OPSWITCH_OUTPUT_ENABLE;
        pDsp[ 0 ]->inputs[ ii ].rset = 0;
        pDsp[ 0 ]->inputs[ ii ].mask = 0;
        pDsp[ 0 ]->inputs[ ii ].control = 0;
    }
    */

    return true;
}

bool Model::Impl::initialize_model_globals()
{
    int status = 0;
    int dcuId;

    // Connect and allocate mbuf memory spaces
    if ( attach_shared_memory( SYSTEM_NAME_STRING_LOWER ) == -1 ) 
    {
        spdlog::error("initialize_model_globals() - Failed to attach to the shared memory.");
        return false;
    }

    // Set global pointer to EPICS shared memory, _epics_shm set by attach_shared_memory
    pLocalEpics = (CDS_EPICS*)&( (RFM_FE_COMMS*)_epics_shm )->epicsSpace;

    // Find and initialize all PCI I/O modules
    // ******************************************************* Following I/O
    // card info is from feCode
    _numPCIeCards = sizeof( cards_used ) / sizeof( cards_used[ 0 ] ); //This looks like we are hardcoding somthing
    spdlog::info( "initialize_model_globals () - configured to use {} cards", _numPCIeCards );
    cdsPciModules.cards = _numPCIeCards;
    cdsPciModules.cards_used = cards_used;
    spdlog::info( "initialize_model_globals() - Initializing PCI Modules..." );
    cdsPciModules.adcCount = 0;
    cdsPciModules.dacCount = 0;
    cdsPciModules.dioCount = 0;
    cdsPciModules.doCount = 0;
    cdsPciModules.rfmCount = 0;
    cdsPciModules.dolphinCount = 0;

    // If running as a control process, I/O card information is via ipc shared
    // memory
    spdlog::info( "initialize_model_globals() - {} PCI cards found", ioMemData->totalCards ); //TODO: Looks like the IOP is setting this

    initmap( &cdsPciModules );
    /// Call PCI initialization routine in map.c file.
    status = mapPciModules( &cdsPciModules );

    // If no ADC cards were found, then control cannot run
    if ( !cdsPciModules.adcCount )
    {
        spdlog::info( "initialize_model_globals() - No ADC cards found - exiting\n" );
        return false;
    }
    spdlog::info( "{} PCI cards found ", status );
    if ( status < _numPCIeCards )
    {
        spdlog::error( "initialize_model_globals() - ERROR **** Did not find correct number of cards! Expected {} "
                "and Found {}",
                _numPCIeCards,
                status );
        cardCountErr = 1;
        //return false;
    }

    // Control app gets RFM module count from MASTER.
    cdsPciModules.rfmCount = ioMemData->rfmCount;
    cdsPciModules.dolphinCount = ioMemData->dolphinCount;

    // Print out all the I/O information
    print_io_info( SYSTEM_NAME_STRING_LOWER, &cdsPciModules, 0 );

    // Initialize buffer for daqLib.c code
    daqBuffer = (long)&daqArea[ 0 ];

    //We set the global run enable
    pLocalEpics->epicsInput.vmeReset = 0;

    /// **********************************************************************************************\n
    /// Start Initialization Process \n
    /// **********************************************************************************************\n

    spdlog::info("initialize_model_globals() - Starting Initialization Process ...\n");

    //memset( tempClock, 0, sizeof( tempClock ) );

    fz_daz( ); /// \> Kill the denorms!


    /// \> Init comms with EPICS processor */
    _pEpicsComms = (RFM_FE_COMMS*)_epics_shm;
    pLocalEpics = (CDS_EPICS*)&(_pEpicsComms->epicsSpace);
    pEpicsDaq = (char*)&( pLocalEpics->epicsOutput );

#ifdef OVERSAMPLE
    /// \> Zero out filter histories
    memset( dHistory, 0, sizeof( dHistory ) );
    memset( dDacHistory, 0, sizeof( dDacHistory ) );
#endif

    clear_dac_outputs();

    /// \> Set pointers to filter module data buffers. \n
    /// - ---- Prior to V2.8, separate local/shared memories for FILT_MOD
    /// data.\n
    /// - ---- V2.8 and later, code uses EPICS shared memory only. This was done
    /// to: \n
    /// - -------- Allow daqLib.c to retrieve filter module data directly from
    /// shared memory. \n
    /// - -------- Avoid copy of filter module data between to memory locations,
    /// which was slow. \n
    pDsp[ 0 ] = (FILT_MOD*)( &_pEpicsComms->dspSpace );
    pCoeff[ 0 ] = (VME_COEF*)( &_pEpicsComms->coeffSpace );
    dspPtr[ 0 ] = (FILT_MOD*)( &_pEpicsComms->dspSpace );

    /// \> Clear the FE reset which comes from Epics
    pLocalEpics->epicsInput.vmeReset = 0;

    // Clear input masks
    pLocalEpics->epicsInput.burtRestore_mask = 0;
    pLocalEpics->epicsInput.dacDuoSet_mask = 0;

    /// < Read in all Filter Module EPICS coeff settings
    for (int ii = 0; ii < MAX_MODULES; ii++ )
    {
        checkFiltReset( ii,
                        dspPtr[ 0 ],
                        pDsp[ 0 ],
                        &_dspCoeff[ 0 ],
                        MAX_MODULES,
                        pCoeff[ 0 ] );
    }

    // Need this FE dcuId to make connection to FB
    // TODO: WTF is this doing?
    dcuId = pLocalEpics->epicsInput.dcuId;
    pLocalEpics->epicsOutput.dcuId = dcuId;

    // Reset timing diagnostics
    pLocalEpics->epicsOutput.diagWord = 0;
    pLocalEpics->epicsOutput.timeDiag = 0;
    pLocalEpics->epicsOutput.timeErr = SYNC_SRC_MASTER;

    if (init_filter_modules() == false)
        return false;


    double cycle_adc_input[MAX_ADC_MODULES][MAX_ADC_CHN_PER_MOD];
    double cycle_dac_output[MAX_DAC_MODULES][MAX_DAC_CHN_PER_MOD];
    iopDacEnable = feCode( 0,
                           cycle_adc_input,
                           cycle_dac_output,
                           dspPtr[ 0 ],
                           &_dspCoeff[ 0 ],
                           (struct CDS_EPICS*)pLocalEpics,
                           1 );


    return true ;
}


//
// Start Interface Model Member functions
//
std::unique_ptr<Model> Model::create_instance()
{

    if (Model::_has_been_created)
    {
        spdlog::error("Model::create_instance() - Only one instance of Model can be crested at once, "
                     "there are shared globals used by the real time model library that would conflict if "
                     "multiple Models are increated in the same process.");
        return nullptr;
    }

    std::unique_ptr<Model> me_ptr (new Model());

    //Load SPDLOG level from env
    spdlog::cfg::load_env_levels();

    me_ptr->_impl = Model::Impl::create_instance();
    if(me_ptr->_impl == nullptr) return nullptr;

    Model::_has_been_created = true;
    return me_ptr;
}

Model::~Model() 
{
    Model::_has_been_created = false;
};

Model::Model() {};

int Model::get_model_rate_Hz()
{
    return _impl->get_model_rate_Hz();
}

int Model::get_num_filter_modules()
{
    return _impl->get_num_filter_modules();
}

bool Model::set_var(const std::string& name, double val)
{
     return _impl->set_var(name, val);
}

bool Model::set_var(const std::string& name, int val)
{
     return _impl->set_var(name, val);
}

bool Model::set_var_momentary(const std::string& name, double val)
{
    return _impl->set_var_momentary(name, val);
}


template <class T>
std::optional<T> Model::get_var(const std::string& name) const
{

    static_assert(std::is_same<T, int>::value ||
                  std::is_same<T, double>::value ||
                  std::is_same<T, char>::value 
                  , "Model::get_var() - Only double, int, and char are supported types.");
    return _impl->get_var<T>(name);
}

// We need to be explicit about the types we are going to support
// Can't put these in the header because _impl is forward declared as
// part of the ptr to impl pattern.
// These need to match the static_assert above, so we push any misuse to 
// compile time.
template class std::optional<int> Model::get_var(const std::string& name) const;
template class std::optional<double> Model::get_var(const std::string& name) const;
template class std::optional<char> Model::get_var(const std::string& name) const;

std::vector<std::string> Model::get_all_input_names()
{
    return _impl->get_all_input_names();
}

std::vector<std::string> Model::get_all_model_var_names()
{
    return _impl->get_all_model_var_names();
}

std::vector<std::string> Model::get_all_filter_names()
{
    return _impl->get_all_filter_names();
}

void Model::set_adc_channel_generator(int adcNum, int chanNum, std::shared_ptr<Generator> gen)
{
    return _impl->set_adc_channel_generator(adcNum, chanNum, gen );
}

bool Model::set_excitation_point_generator(const std::string & name, std::shared_ptr<Generator> gen)
{
    return _impl->set_excitation_point_generator(name, gen);
}


bool Model::set_ipc_receiver_generator(const std::string & name, std::shared_ptr<Generator> gen)
{
    return _impl->set_ipc_receiver_generator(name, gen);
}

void Model::record_dac_output(bool shouldRec)
{
    _impl->record_dac_output(shouldRec);
}

bool Model::record_model_var(const std::string & name, int record_rate_hz)
{
    return _impl->record_model_var(name, record_rate_hz);
}

template< class T>
std::optional<std::vector<T>> Model::get_recorded_var(const std::string & name) const
{
    static_assert(std::is_same<T, int>::value ||
                  std::is_same<T, double>::value ||
                  std::is_same<T, char>::value
                  , "Model::get_recorded_var() - Only double, int, and char are supported types.");
    return _impl->get_recorded_var<T>(name);
}
// See note above at getVar()
template class std::optional<std::vector<int>> Model::get_recorded_var(const std::string& name) const;
template class std::optional<std::vector<double>> Model::get_recorded_var(const std::string& name) const;
template class std::optional<std::vector<char>> Model::get_recorded_var(const std::string& name) const;

const std::vector< double > & Model::get_dac_output_by_id(int dacIndex, int chanIndex)
{
    return _impl->get_dac_output_by_id(dacIndex, chanIndex);
}

int Model::run_model(unsigned num_cycles)
{
    return _impl->run_model(num_cycles);
}

std::optional<LIGO_FilterModule> Model::get_filter_module_by_name(const std::string & name)
{
    return _impl->get_filter_module_by_name(name);
}

std::optional<LIGO_FilterModule> Model::get_filter_module_by_id(int module_index)
{
    return _impl->get_filter_module_by_id(module_index);
}


bool Model::reset_filter_module(const std::string & name)
{
    return _impl->reset_filter_module(name);
}

bool Model::load_biquad_coefficients(const std::string & filter_name,
                                   const std::vector<int> & stageIndicesToLoad,
                                   const std::vector<double> & coefficients)
{
    return _impl->load_biquad_coefficients(filter_name, stageIndicesToLoad, coefficients);
}

bool Model::load_scipy_sos_coef(const std::string & filter_name,
                                  const std::vector<int> & stageIndicesToLoad,
                                  const std::vector< std::vector<double> > & sections_and_coefficients)
{
    return _impl->load_scipy_sos_coef(filter_name, stageIndicesToLoad, sections_and_coefficients);
}

