#define CATCH_CONFIG_MAIN
#include <catch.hpp>


#include "Model.hh"
#include "ConstantGenerator.hh"
#include "GaussianNoiseGenerator.hh"
#include "RampGenerator.hh"

#include <iostream>
#include <cmath>

TEST_CASE( "1 second ramp test", "[cdsfilters]" )
{

    constexpr int sample_value = 1000;

    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    const int rate_hz =  model_ptr->get_model_rate_Hz();
    const int num_cycles = rate_hz * 2; //Run for 2 periods

    std::vector<std::string> filter_names = model_ptr->get_all_filter_names();
    REQUIRE(filter_names.size() == model_ptr->get_num_filter_modules());

    //Configure Input
    model_ptr->set_adc_channel_generator( 0, 0, std::make_shared<rts::ConstantGenerator>( sample_value ) );

    //Configure modules
    //
    std::optional<rts::LIGO_FilterModule> filter1 = model_ptr->get_filter_module_by_name("FM0");
    REQUIRE( filter1 );
    filter1.value().enable_input().enable_output().set_gain(1).set_ramp_time_s(1.0);

    //Configure record points
    //
    model_ptr->record_dac_output(true);

    //Run Model
    //
    model_ptr->run_model(num_cycles);

    //Examine Outputs
    const std::vector<double> & dac_chan = model_ptr->get_dac_output_by_id(0,0);

    //auto myfile = std::fstream("dac_0_0.doubles", std::ios::out | std::ios::binary);
    //myfile.write((char*)&dac_chan[0], dac_chan.size() * sizeof(double));
    //myfile.close();


    int num_samps = 0;
    for ( const auto & sample : dac_chan )
    {
        if( sample >= sample_value)
            break;
        ++num_samps;
    }

    //Ramp time is one sec, so we expect the signal to ramp up in 1 period
    const double expected_val = rate_hz; 
    INFO("Measured val was " << num_samps << ", expected " << expected_val);

    double per_error = std::fabs((num_samps-expected_val)/expected_val)*100;
    REQUIRE(per_error < 2.0);

}


TEST_CASE( "100 second ramp down test", "[cdsfilters]" )
{

    constexpr int sample_value = 1000;

    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    const int rate_hz =  model_ptr->get_model_rate_Hz();
    const int num_cycles = rate_hz * 150; 

    std::vector<std::string> filter_names = model_ptr->get_all_filter_names();
    REQUIRE(filter_names.size() == model_ptr->get_num_filter_modules());

    //Set inputs
    //
    model_ptr->set_adc_channel_generator( 0, 0, std::make_shared<rts::ConstantGenerator>(sample_value) );

    //Configure modules
    //
    std::optional<rts::LIGO_FilterModule> filter1 = model_ptr->get_filter_module_by_name("FM0");
    REQUIRE( filter1 );
    filter1.value().enable_input().enable_output().set_gain(1).set_ramp_time_s(100);

    //Configure record points
    //
    model_ptr->record_dac_output(true);

    //Run Model
    //
    model_ptr->run_model(num_cycles);

    //Examine Outputs
    const std::vector<double> & dac_chan = model_ptr->get_dac_output_by_id(0,0);

    //auto myfile = std::fstream("test2_dac_0_0.doubles", std::ios::out | std::ios::binary);
    //myfile.write((char*)&dac_chan[0], dac_chan.size() * sizeof(double));
    //myfile.close();


    int num_samps = 0;
    for ( const auto & sample : dac_chan )
    {
        if( sample >= sample_value)
            break;
        ++num_samps;
    }

    //Ramp time is 100 sec, so we expect the signal to ramp up in 100 periods
    const double expected_val = rate_hz*100;
    INFO("Found val after num_samps: " << num_samps << ", expected : " << expected_val);


    double per_error = std::fabs((num_samps-expected_val)/expected_val)*100;
    REQUIRE(per_error < 2.0);

}

TEST_CASE( "_OUT TP Test", "[cdsfilters]" )
{

    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    const int num_cycles = model_ptr->get_model_rate_Hz() * 2; //Run for 2 periods


    //Configure Input
    model_ptr->set_adc_channel_generator( 0, 0, std::make_shared<rts::GaussianNoiseGenerator>( 0, 1 ) );

    //Configure modules
    //
    std::optional<rts::LIGO_FilterModule> filter1 = model_ptr->get_filter_module_by_name("FM0");
    REQUIRE( filter1 );
    filter1.value().enable_input().enable_output().set_gain(1).set_ramp_time_s(0);

    //Configure record points
    //
    model_ptr->record_dac_output(true);
    model_ptr->record_model_var("FM0_OUT", model_ptr->get_model_rate_Hz());

    //Run Model
    //
    model_ptr->run_model(num_cycles);

    //Examine Outputs
    const std::vector<double> & dac_chan_0 = model_ptr->get_dac_output_by_id(0,0);
    const std::optional<std::vector< double > > fm0_out_tp = model_ptr->get_recorded_var<double>("FM0_OUT");

    REQUIRE(dac_chan_0.size() == fm0_out_tp.value().size());


    for(int i = 0; i < dac_chan_0.size(); ++i)
    {
        REQUIRE(dac_chan_0[i] == fm0_out_tp.value()[i]);
    }

}

TEST_CASE( "_INX TP Test", "[cdsfilters]" )
{

    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    const int num_cycles = model_ptr->get_model_rate_Hz() * 2; //Run for 2 periods


    //Configure Input
    model_ptr->set_adc_channel_generator( 0, 0, std::make_shared<rts::RampGenerator>( 0, 1 ) );

    model_ptr->set_excitation_point_generator( "FM0_EXC", std::make_shared<rts::RampGenerator>(0, 1) );

    //Configure modules
    //
    std::optional<rts::LIGO_FilterModule> filter1 = model_ptr->get_filter_module_by_name("FM0");
    REQUIRE( filter1 );
    filter1.value().enable_input().enable_output().set_gain(1).set_ramp_time_s(0);

    //Configure record points
    //
    //model_ptr->record_dac_output(true);
    model_ptr->record_model_var("FM0_IN1", model_ptr->get_model_rate_Hz());
    model_ptr->record_model_var("FM0_IN2", model_ptr->get_model_rate_Hz());

    //Run Model
    //
    model_ptr->run_model(num_cycles);

    //Examine Outputs
    //const std::vector<double> & dac_chan_0 = model_ptr->get_dac_output_by_id(0,0);
    const std::optional<std::vector< double > > in1_tp = model_ptr->get_recorded_var<double>("FM0_IN1");
    const std::optional<std::vector< double > > in2_tp = model_ptr->get_recorded_var<double>("FM0_IN2");

    REQUIRE(in1_tp.value().size() == in2_tp.value().size());


    for(int i = 0; i < in1_tp.value().size(); ++i)
    {
        INFO("i = " << i );
        REQUIRE(in1_tp.value()[i]*2 == in2_tp.value()[i]);
        //in2_tp shoudl be ramp exc plus ramp input so 2*in1_tp
        
    }

}