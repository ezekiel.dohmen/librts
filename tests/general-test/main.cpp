#define CATCH_CONFIG_MAIN
#include <catch.hpp>


#include "Model.hh"
#include "ConstantGenerator.hh"
#include "RampGenerator.hh"


#include <iostream>
#include <cmath>

TEST_CASE( "Double creation", "[general]" )
{
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    std::unique_ptr<rts::Model> model2_ptr = rts::Model::create_instance();
    REQUIRE( model2_ptr == nullptr);

}

TEST_CASE( "excitation test", "[general]" )
{
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    model_ptr->set_excitation_point_generator( "TEST_EXCITATION", std::make_shared<rts::RampGenerator>(0, 1) );

    model_ptr->record_model_var("EXCITATION_EPICS_OUT", model_ptr->get_model_rate_Hz());

    model_ptr->run_model(model_ptr->get_model_rate_Hz() * 1);

    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("EXCITATION_EPICS_OUT");
    REQUIRE(epics_out);

    double sample = epics_out.value()[0];
    for( unsigned i = 1; i < epics_out.value().size(); ++i)
    {
        REQUIRE( sample == epics_out.value()[i] - 1 );
        sample = epics_out.value()[i];
    }
}

TEST_CASE( "ipc sender test", "[general]" )
{
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    model_ptr->set_excitation_point_generator( "IPC_EXCITATION", std::make_shared<rts::RampGenerator>(0, 1) );

    model_ptr->record_model_var("X1:SHMEM_SND_ONLY", model_ptr->get_model_rate_Hz());

    model_ptr->run_model(model_ptr->get_model_rate_Hz() * 1);

    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("X1:SHMEM_SND_ONLY");
    REQUIRE(epics_out);

    double sample = epics_out.value()[0];
    for( unsigned i = 1; i < epics_out.value().size(); ++i)
    {
        INFO("i = " << i);
        REQUIRE( sample == epics_out.value()[i] - 1 );
        sample = epics_out.value()[i];
    }
}


TEST_CASE( "ipc receiver test", "[general]" )
{
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    model_ptr->set_ipc_receiver_generator( "X1:RECV_ONLY", std::make_shared<rts::RampGenerator>(0, 1) );

    model_ptr->record_model_var("IPC_RECV_ONLY_TP", model_ptr->get_model_rate_Hz());

    model_ptr->run_model(model_ptr->get_model_rate_Hz() * 1);

    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("IPC_RECV_ONLY_TP");
    REQUIRE(epics_out);

    double sample = epics_out.value()[0];
    for( unsigned i = 1; i < epics_out.value().size(); ++i)
    {
        INFO("i = " << i);
        REQUIRE( sample == epics_out.value()[i] - 1 );
        sample = epics_out.value()[i];
    }
}


