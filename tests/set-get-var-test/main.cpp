#define CATCH_CONFIG_MAIN
#include <catch.hpp>


#include "Model.hh"
#include "RampGenerator.hh"

#include <iostream>
#include <cmath>

TEST_CASE( "Set and get a model var", "[set], [get]" )
{


    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE(model_ptr);

    //Set inputs
    //
    model_ptr->set_adc_channel_generator( 0, 30, std::make_shared<rts::RampGenerator>(0, 1) );

    //Set values
    //
    REQUIRE( model_ptr->set_var("DAC_15_EPICS_INPUT", 99.56) );
    REQUIRE( model_ptr->get_var<double>("DAC_15_EPICS_INPUT") == Approx( 99.56 ) );

    REQUIRE( model_ptr->set_var("VME_RESET", 555) );
    REQUIRE( model_ptr->get_var<int>("VME_RESET") == 555 );

    REQUIRE( model_ptr->set_var("DAC_15_EPICS_INPUT_MASK", 120) );
    REQUIRE( (int)model_ptr->get_var<char>("DAC_15_EPICS_INPUT_MASK").value() == 120 );

    REQUIRE( model_ptr->get_var<double>("DAC_15_EPICS_INPUT") == Approx( 99.56 ) );

    //Configure record points
    //
    model_ptr->record_dac_output(true);

    //Run Model
    //
    model_ptr->run_model(model_ptr->get_model_rate_Hz()*1);//Run for 1 sec

    const std::vector<double> & dac_chan = model_ptr->get_dac_output_by_id(0,15);
    REQUIRE( dac_chan[0] == Approx( 99.56 ) ); //Make sure the value has propagated to the dac update

    REQUIRE( model_ptr->get_var<double>("DAC_15_EPICS_INPUT") == Approx( 99.56 ) );

}


TEST_CASE( "Set variable momentarily", "[set_var_momentary]" )
{


    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE(model_ptr);



    //Set inputs
    //
    model_ptr->set_adc_channel_generator( 0, 30, std::make_shared<rts::RampGenerator>(0, 1) );

    //Set values
    //
    model_ptr->set_var_momentary("MOMENTARY_TEST", 1);

    model_ptr->set_var("EX_EPICS_BIN_IN", 99);
    model_ptr->set_var_momentary("EX_EPICS_BIN_IN", 1);


    //Configure record points
    //
    model_ptr->record_model_var("MOMENTARY_TEST_OUT", model_ptr->get_model_rate_Hz());
    //While EX_EPICS_BIN_OUT is not a momentary EPICS variable, the library can still handle it
    model_ptr->record_model_var("EX_EPICS_BIN_OUT", model_ptr->get_model_rate_Hz());

    //Run Model
    //
    model_ptr->run_model( 5 );


    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("EX_EPICS_BIN_OUT");
    REQUIRE(epics_out);
    REQUIRE(epics_out.value().size() == 5);
    REQUIRE( epics_out.value()[0] == Approx( 1) );
    INFO("epics_out.value()[0] = " << epics_out.value()[0]);
    for(int i = 0; i < epics_out.value().size(); ++i)
        REQUIRE( epics_out.value()[1] == Approx( 99 ) );

    std::optional< std::vector<double> > mom_epics_out = model_ptr->get_recorded_var<double>("MOMENTARY_TEST_OUT");
    REQUIRE(mom_epics_out);
    REQUIRE(mom_epics_out.value().size() == 5);
    REQUIRE( mom_epics_out.value()[0] == Approx( 1 ) );
    INFO("mom_epics_out.value()[0] = " << mom_epics_out.value()[0]);
    for(int i = 0; i < mom_epics_out.value().size(); ++i)
        REQUIRE( mom_epics_out.value()[1] == Approx( 0.0 ) );


}
