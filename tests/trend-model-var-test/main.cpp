#define CATCH_CONFIG_MAIN
#include <catch.hpp>


#include "Model.hh"
#include "RampGenerator.hh"

#include <iostream>
#include <cmath>

TEST_CASE( "Trend 10 times a sec for one sec", "[trendvars]" )
{


    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE(model_ptr);

    //Set inputs
    //
    model_ptr->set_adc_channel_generator( 0, 30, std::make_shared<rts::RampGenerator>(0, 1) );

    //Configure modules
    //
    model_ptr->record_model_var("RAMP_EPICS_OUT", 10);


    //Configure record points
    //
    model_ptr->record_dac_output(false);

    //Run Model
    //
    model_ptr->run_model(model_ptr->get_model_rate_Hz()*1);//Run for 1 sec

    //Examine Recorded Outputs
    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("RAMP_EPICS_OUT");
    REQUIRE(epics_out);

    REQUIRE( epics_out.value().size() == 10 );

}


TEST_CASE( "Trend 2048 times a sec for 20 sec", "[trendvars]" )
{


    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE(model_ptr);

    //Set inputs
    //
    model_ptr->set_adc_channel_generator( 0, 30, std::make_shared<rts::RampGenerator>(0, 1) );

    //Configure modules
    //
    model_ptr->record_model_var("RAMP_EPICS_OUT", 2048);


    //Configure record points
    //
    model_ptr->record_dac_output(false);

    //Run Model
    //
    model_ptr->run_model(model_ptr->get_model_rate_Hz()*20);//Run for 20 sec

    //Examine Recorded Outputs
    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("RAMP_EPICS_OUT");
    REQUIRE(epics_out);

    REQUIRE( epics_out.value().size() ==  20*2048);

}

TEST_CASE( "Trend testpoint", "[trendvars]" )
{


    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE(model_ptr);

    //Set inputs
    //
    model_ptr->set_adc_channel_generator( 0, 31, std::make_shared<rts::RampGenerator>(0, 1) );

    //Configure modules
    //
    model_ptr->record_model_var("TP_TEST", model_ptr->get_model_rate_Hz());


    //Run Model
    //
    model_ptr->run_model(model_ptr->get_model_rate_Hz()*2);//Run for 2 sec

    //Examine Recorded Outputs
    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("TP_TEST");
    REQUIRE(epics_out);

    REQUIRE( epics_out.value().size() ==  2*2048);

}
